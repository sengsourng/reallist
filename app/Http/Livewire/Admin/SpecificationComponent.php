<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithPagination;
use App\Models\Specification;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Pagination\Paginator;

class SpecificationComponent extends Component
{
    // public $specifications;
    public  $name, $slug, $icon, $image,$short_description,$specification_id;
    public $isModalOpen = 0;
    public $pagination;
    use WithPagination;

    public $updateMode=0;


    // public $Paginator=Paginator::defaultView();
    // public $paginator=$specifications;
    protected $paginationTheme = 'bootstrap';


    public $search = '';
    public $pagesNo;


    public function updatingSearch()
    {
        $this->resetPage();
    }

    // Update slug auto

    public function updatedName($field)
    {
       $this->slug = Str::slug($field);
    }

    public function render()
    {

        // livewire.admin.specification.home
        return view('livewire.admin.specification-component', [
            // 'specifications' => Specification::paginate(2),
            'specifications' => Specification::where('name', 'like', '%'.$this->search.'%')->paginate($this->pagesNo),

        ])->layout('layouts.backend');


    }

    private function resetInputFields(){
        $this->name = '';
        $this->slug = '';
        $this->icon = '';
        $this->image = '';
        $this->short_description = '';
    }


    public function create()
    {
        $this->resetInputFields();
        $this->updateMode=false;
    }


    public function store()
    {

        $this->validate([
            'name' => 'required',
            'slug' => 'required',
        ]);

        $dataStore=[
            'name' => $this->name,
            'slug' => $this->slug,
            'icon' => $this->icon,
            'image' => $this->image,
            'short_description' => $this->short_description,
        ];
        Specification::create($dataStore);
        session()->flash('message', 'Users Created Successfully.');
        $this->resetInputFields();
        $this->emit('userStore'); // Close model to using to jquery
        session()->flash('message', 'Your data was saved successfull!');

    }

    public function edit($id)
    {
        $this->updateMode = true;
        $specification = Specification::findOrFail($id);
        $this->specification_id = $id;
        $this->name = $specification->name;
        $this->slug = $specification->slug;
        $this->icon = $specification->icon;
        $this->image = $specification->image;
        $this->short_description = $specification->short_description;

    }


    public function update()
    {
        $this->validate([
            'name' => 'required',
            'slug' => 'required',
            // 'mobile' => 'required',
        ]);

        if($this->updateMode==true){
            if ($this->specification_id) {
                $specification = Specification::find($this->specification_id);
                $specification->update([
                'name' => $this->name,
                'slug' => $this->slug,
                'icon' => $this->icon,
                'image' => $this->image,
                'short_description' => $this->short_description,
                ]);

                $this->updateMode = false;

            }
        }

        $this->emit('userStore'); // Close model to using to jquery
        session()->flash('message', 'Your data updated successfully!');

    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try
        {
            /*...Your code ... */
            Specification::find($id)->delete();

            session()->flash('message', 'Specification deleted.');

            // $this->dispatchBrowserEvent('closeModal');
            $this->emit('confirm');

        } catch (\Throwable $th) {

            DB::rollBack();
            $this->emit('confirm'); // Close modal "confirm"
            session()->flash('error', 'You can not delete this!');

        }


    }






}
