<?php

namespace App\Http\Livewire\Admin;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserListComponent extends Component
{
    public  $name, $first_name, $last_name, $email,$password,$phone,$about_me,$utype,$user_id;
    public $position;
    // Address
    public $address,$city,$country,$postal_code;
    public $pagination;
    use WithPagination;

    public $updateMode=0;

    protected $paginationTheme = 'bootstrap';


    public $search = '';
    public $pagesNo=4;

    public function render()
    {

        return view('livewire.admin.user-list-component', [
            // 'specifications' => Specification::paginate(2),
            'users' => User::where('name', 'like', '%'.$this->search.'%')->paginate($this->pagesNo),
        ])->layout('layouts.backend');

        // return view('livewire.admin.user-list-component')->layout('layouts.backend');
    }




    private function resetInputFields(){
        $this->name= '';
        $this->first_name= '';
        $this->last_name= '';
        $this->email= '';
        $this->password= '';
        $this->position= '';
        $this->phone= '';
        $this->address= '';
        $this->postal_code= '';
        $this->city= '';
        $this->country= '';
        $this->profile_photo_path= '';
        $this->about_me= '';
        $this->utype= '';

    }


    public function create()
    {
        $this->resetInputFields();
        $this->updateMode=false;
    }


    public function store()
    {

        $this->validate([
            'name' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'position' => 'required',
            'phone' => 'required',
            'utype'=> 'required',
        ]);

        $dataStore=[
            'name' => $this->name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'password' => Hash::make($this->password),
            'position' => $this->position,
            'phone' => $this->phone,
            'address' => $this->address,
            'about_me' => $this->about_me,

            'utype' => $this->utype !=NULL?$this->utype:"USR",
        ];
        User::create($dataStore);
        session()->flash('message', 'Users Created Successfully.');
        $this->resetInputFields();
        $this->emit('userStore'); // Close model to using to jquery
        session()->flash('message', 'Your data was saved successfull!');

    }

    public function edit($id)
    {
        $this->updateMode = true;
        $user = User::findOrFail($id);
        $this->user_id = $id;
        $this->name= $user->name;
        $this->first_name= $user->first_name;
        $this->last_name= $user->last_name;
        $this->email= $user->email;
        $password = bcrypt($user->password);
        $this->password= $password;
        $this->position= $user->position;
        $this->phone= $user->phone;
        $this->address= $user->address;
        $this->postal_code= $user->postal_code;
        $this->city= $user->city;
        $this->country= $user->country;
        $this->profile_photo_path= $user->profile_photo_path;
        $this->about_me= $user->about_me;
        $this->utype= $user->utype;

    }


    public function update()
    {
        $this->validate([
            'name' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            // 'mobile' => 'required',
        ]);

        if($this->updateMode==true){
            if ($this->user_id) {
                $user = User::find($this->user_id);
                $user->update([
                    'name' => $this->name,
                    'first_name' => $this->first_name,
                    'last_name' => $this->last_name,
                    'email' => $this->email,
                    'password' => Hash::make($this->password),
                    'position' => $this->position,
                    'phone' => $this->phone,
                    'address' => $this->address,
                    'about_me' => $this->about_me,

                    'utype' => $this->utype,
                ]);

                $this->updateMode = false;

            }
        }

        $this->emit('userStore'); // Close model to using to jquery
        session()->flash('message', 'Your data updated successfully!');

    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try
        {
            /*...Your code ... */
            User::find($id)->delete();

            session()->flash('message', 'User deleted.');

            // $this->dispatchBrowserEvent('closeModal');
            $this->emit('confirm');

        } catch (\Throwable $th) {

            DB::rollBack();
            $this->emit('confirm'); // Close modal "confirm"
            session()->flash('error', 'You can not delete this!');

        }

    }


}
