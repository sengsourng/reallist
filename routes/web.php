<?php

use App\Http\Livewire\Admin\AdminDashboardComponent;
use App\Http\Livewire\Admin\SpecificationComponent;
use App\Http\Livewire\Admin\UserListComponent;
use App\Http\Livewire\BuyComponent;
use App\Http\Livewire\HomeComponent;
use App\Http\Livewire\RentComponent;
use App\Http\Livewire\User\UserDashboardComponent;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () {
    // return view('welcome');
    // return view('layouts.backend');

});


Route::get('/',HomeComponent::class);
Route::get('/buy', BuyComponent::class);
Route::get('/rent',RentComponent::class);

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
//     // Route::get('/',HomeComponent::class);
// })->name('dashboard');

// For User or Customer
Route::middleware(['auth:sanctum', 'verified'])->group(function(){
    Route::get('/user/dashboard',UserDashboardComponent::class)->name('user.dashboard');
});


// For Admin
Route::middleware(['auth:sanctum', 'verified','authadmin'])->group(function(){
    Route::get('/admin/dashboard',AdminDashboardComponent::class)->name('admin.dashboard');
    Route::get('/admin/user-list',UserListComponent::class)->name('admin.user-list');
    // admin.specifications
    Route::get('/admin/specifications',SpecificationComponent::class)->name('admin.specifications');

});
