<!doctype html>
<html lang="en" dir="ltr">
	<head>
		<!-- Meta data -->
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="Reallist- Bootstrap Responsive Real estate Classifieds, Dealer, Rentel, Builder and Agent Multipurpose HTML Template" name="description">
		<meta content="Spruko Technologies Private Limited" name="author">
		<meta name="keywords" content="html template, real estate websites, real estate html template, property websites, premium html templates, real estate company website, bootstrap real estate template, real estate marketplace html template, listing website template, property listing html template, real estate bootstrap template, real estate html5 template, real estate listing template, property template, property dealer website"/>

		<!-- Favicon -->
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

		<!-- Title -->
		<title>Reallist– Clean & Modern Admin Dashboard Bootstrap 4 HTML Template</title>
		<link rel="stylesheet" href="{{asset('assets/fonts/fonts/font-awesome.min.css')}}">

		<!-- Bootstrap Css -->
		<link href="{{asset('assets/plugins/bootstrap-4.3.1-dist/css/bootstrap.min.css')}}" rel="stylesheet" />

		<!-- Dashboard Css -->
		<link href="{{asset('assets/css/style.css')}}" rel="stylesheet" />
		<link href="{{asset('assets/css/admin-custom.css')}}" rel="stylesheet" />

		<!-- Sidemenu Css -->
		<link href="{{asset('assets/css/sidemenu.css')}}" rel="stylesheet" />
		<!-- p-scroll bar css-->
		<link href="{{asset('assets/plugins/pscrollbar/pscrollbar.css')}}" rel="stylesheet" />

		<!---Font icons-->
		<link href="{{asset('assets/css/icons.css')}}" rel="stylesheet"/>
		<link href="{{asset('assets/plugins/iconfonts/icons.css')}}" rel="stylesheet" />

        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Hanuman:wght@400;700&display=swap" rel="stylesheet">

        <style>
            body{
                font-family: 'Hanuman', 'serif' !important;
            }
        </style>

        @stack('css')

        @livewireStyles


	</head>
	<body class="app sidebar-mini">

		<div id="global-loader">
			<img src="../assets/images/loader.svg" class="loader-img" alt="">
		</div>

		<div class="page">
			<div class="page-main h-100">
				@include('layouts.includes.admin_top_navbar')

                {{-- Sidebar menu --}}
                @include('layouts.includes.admin_sidebar')


				<div class="app-content  my-3 my-md-5">

                        {{-- Dashboard Or Form page --}}
                        {{-- @include('admin.dashboard.dashboard') --}}

                        {{ $slot }}


                </div>




			</div>


			<!--footer-->
			<footer class="footer">
				<div class="container">
					<div class="row align-items-center flex-row-reverse">
						<div class="col-md-12 col-sm-12 mt-3 mt-lg-0 text-center">
							Copyright © 2019 <a href="#">Reallist</a>. Designed by <a href="#">Spruko</a> All rights reserved.
						</div>
					</div>
				</div>
			</footer>
			<!-- End Footer-->
		</div>

		<!-- Back to top -->
		<a href="#top" id="back-to-top" ><i class="fa fa-rocket"></i></a>


		<!-- Dashboard Core -->
		<script src="{{asset('assets/js/vendors/jquery-3.2.1.min.js')}}"></script>
		<script src="{{asset('assets/plugins/bootstrap-4.3.1-dist/js/popper.min.js')}}"></script>
		<script src="{{asset('assets/plugins/bootstrap-4.3.1-dist/js/bootstrap.min.js')}}"></script>
		<script src="{{asset('assets/js/vendors/jquery.sparkline.min.js')}}"></script>
		<script src="{{asset('assets/js/vendors/selectize.min.js')}}"></script>
		<script src="{{asset('assets/js/vendors/jquery.tablesorter.min.js')}}"></script>
		<script src="{{asset('assets/js/vendors/circle-progress.min.js')}}"></script>
		<script src="{{asset('assets/plugins/rating/jquery.rating-stars.js')}}"></script>

		<!--Counters -->
		<script src="{{asset('assets/plugins/counters/counterup.min.js')}}"></script>
		<script src="{{asset('assets/plugins/counters/waypoints.min.js')}}"></script>
		<!-- Fullside-menu Js-->
		<script src="{{asset('assets/plugins/toggle-sidebar/sidemenu.js')}}"></script>

		<!-- CHARTJS CHART -->
		<script src="{{asset('assets/plugins/chart/Chart.bundle.js')}}"></script>
		<script src="{{asset('assets/plugins/chart/utils.js')}}"></script>

		<!-- p-scroll bar Js-->
		<script src="{{asset('assets/plugins/pscrollbar/pscrollbar.js')}}"></script>
		<script src="{{asset('assets/plugins/pscrollbar/pscroll.js')}}"></script>

		<!-- ECharts Plugin -->
		<script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script>
		<script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script>
		<script src="{{asset('assets/js/index1.js')}}"></script>

		<!-- Custom Js-->
		<script src="{{asset('assets/js/admin-custom.js')}}"></script>
        @include('sweetalert::alert')

        @stack('js')
        @livewireScripts


        <script type="text/javascript">
            window.livewire.on('userStore', () => {
                $('#openModal').modal('hide');
            });

            window.livewire.on('userStore', () => {
                $('#editModal').modal('hide');
            });

        </script>


	</body>
</html>
