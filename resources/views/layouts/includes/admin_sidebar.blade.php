<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
				<aside class="app-sidebar doc-sidebar">
					<div class="app-sidebar__user clearfix">
						<div class="dropdown user-pro-body">
							<div>
								<img src="../assets/images/faces/male/25.jpg" alt="user-img" class="avatar avatar-lg brround">
								<a href="editprofile.html" class="profile-img">
									<span class="fa fa-pencil" aria-hidden="true"></span>
								</a>
							</div>
							<div class="user-info">
								<h2>{{Auth::user()->name}}</h2>
								<span>{{Auth::user()->position}}</span>
							</div>
						</div>
					</div>
					<ul class="side-menu">
						{{-- <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="{{route('admin.dashboard')}}"><i class="side-menu__icon fa fa-tachometer"></i><span class="side-menu__label">Dashboard</span></a>

						</li> --}}
                        <li>
							<a class="side-menu__item" href="{{route('admin.dashboard')}}"><i class="side-menu__icon fa fa-tachometer"></i><span class="side-menu__label">Dashboard</span></a>
						</li>

						<li>
							<a class="side-menu__item" href="widgets.html"><i class="side-menu__icon fa fa-map-marker"></i><span class="side-menu__label">Locations</span></a>
						</li>
                        {{-- <i class="fas fa-search-location"></i> --}}
                        <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-home"></i><span class="side-menu__label">Properties</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="calendar.html" class="slide-item">Property List</a>
								</li>
								<li>
									<a href="calendar2.html" class="slide-item">Property Types</a>
								</li>

                                <li>
									<a href="{{route('admin.specifications')}}" class="slide-item">Specifications</a>
								</li>
							</ul>
						</li>


                        <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-group"></i><span class="side-menu__label">Users</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="{{route('admin.user-list')}}" class="slide-item">User List</a>
								</li>
								<li>
									<a href="calendar2.html" class="slide-item">User Types</a>
								</li>
							</ul>
						</li>




                    </ul>
					<div class="app-sidebar-footer">
						<a href="emailservices.html">
							<span class="fa fa-envelope" aria-hidden="true"></span>
						</a>
						<a href="profile.html">
							<span class="fa fa-user" aria-hidden="true"></span>
						</a>
						<a href="editprofile.html">
							<span class="fa fa-cog" aria-hidden="true"></span>
						</a>
						<a href="{{route('login')}}l">
							<span class="fa fa-sign-in" aria-hidden="true"></span>
							</a>
						<a href="chat.html">
							<span class="fa fa-comment" aria-hidden="true"></span>
						</a>
					</div>
				</aside>
