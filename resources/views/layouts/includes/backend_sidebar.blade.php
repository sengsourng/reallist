<div id="sidebar" class="sidebar sidebar-fixed expandable sidebar-light">
    <div class="sidebar-inner">

      <div class="ace-scroll flex-grow-1" data-ace-scroll="{}">

        <div class="sidebar-section my-2">
          <!-- the shortcut buttons -->
          <div class="sidebar-section-item fadeable-left">
            <div class="fadeinable sidebar-shortcuts-mini">
              <!-- show this small buttons when collapsed -->
              <span class="btn btn-success p-0 opacity-1"></span>
              <span class="btn btn-info p-0 opacity-1"></span>
              <span class="btn btn-orange p-0 opacity-1"></span>
              <span class="btn btn-danger p-0 opacity-1"></span>
            </div>

            <div class="fadeable">
              <!-- show this small buttons when not collapsed -->
              <div class="sub-arrow"></div>
              <div>
                <button class="btn px-25 py-2 text-95 btn-success opacity-1">
                  <i class="fa fa-signal f-n-hover"></i>
                </button>

                <button class="btn px-25 py-2 text-95 btn-info opacity-1">
                  <i class="fa fa-edit f-n-hover"></i>
                </button>

                <button class="btn px-25 py-2 text-95 btn-orange opacity-1">
                  <i class="fa fa-users f-n-hover"></i>
                </button>

                <button class="btn px-25 py-2 text-95 btn-danger opacity-1">
                  <i class="fa fa-cogs f-n-hover"></i>
                </button>
              </div>
            </div>
          </div>
        </div>

        <ul class="nav has-active-border active-on-right">


          <li class="nav-item active">

            <a href="{{route('admin.dashboard')}}" class="nav-link">
              <i class="nav-icon fa fa-tachometer-alt"></i>
              <span class="nav-text fadeable">
               <span>Dashboard</span>
              </span>
            </a>
            <b class="sub-arrow"></b>

          </li>


          <li class="nav-item">

            <a href="#" class="nav-link dropdown-toggle collapsed">
              <i class="nav-icon fa fa-cube"></i>
              <span class="nav-text fadeable">
               <span>Properties</span>
              </span>
              <b class="caret fa fa-angle-left rt-n90"></b>

            </a>
            <div class="hideable submenu collapse">
              <ul class="submenu-inner">
                <li class="nav-item">
                  <a href="{{route('admin.dashboard')}}" class="nav-link">
                    <span class="nav-text">
                           <span>All Properties</span>
                    </span>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{route('admin.dashboard')}}" class="nav-link">
                    <span class="nav-text">
                           <span>Property Types</span>
                    </span>
                  </a>
                </li>

                <li class="nav-item">
                  <a href="{{route('admin.specifications')}}" class="nav-link">
                    <span class="nav-text">
                           <span>Specifications</span>
                    </span>
                  </a>
                </li>
              </ul>
            </div>

            <b class="sub-arrow"></b>

          </li>


          <li class="nav-item">

            <a href="#" class="nav-link dropdown-toggle collapsed">
              <i class="nav-icon fa fa-desktop"></i>
              <span class="nav-text fadeable">
               <span>Users</span>
              </span>
              <b class="caret fa fa-angle-left rt-n90"></b>
            </a>

            <div class="hideable submenu collapse">
              <ul class="submenu-inner">
                <li class="nav-item">
                  <a href="{{route('admin.user-list')}}" class="nav-link">
                    <span class="nav-text">
                           <span>All Users</span>
                    </span>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{route('admin.user-list')}}" class="nav-link">
                    <span class="nav-text">
                           <span>User Type</span>
                    </span>
                  </a>
                </li>





              </ul>
            </div>

            <b class="sub-arrow"></b>

          </li>


          <li class="nav-item">
            <a href="#" class="nav-link dropdown-toggle collapsed">
              <i class="nav-icon fa fa-table"></i>
              <span class="nav-text fadeable">
               <span>Marketing</span>
              </span>

              <b class="caret fa fa-angle-left rt-n90"></b>
            </a>

            <div class="hideable submenu collapse">
              <ul class="submenu-inner">

                <li class="nav-item">
                  <a href="html/table-basic.html" class="nav-link">
                    <span class="nav-text">
                           <span>Market Active</span>
                    </span>
                  </a>
                </li>


                <li class="nav-item">
                  <a href="html/table-datatables.html" class="nav-link">
                    <span class="nav-text">
                           <span>Market Closed</span>
                    </span>
                  </a>
                </li>


                <li class="nav-item">
                  <a href="html/table-bootstrap.html" class="nav-link">
                    <span class="nav-text">
                           <span>Market Boosting</span>
                    </span>
                  </a>
                </li>


                <li class="nav-item">
                  <a href="html/table-jqgrid.html" class="nav-link">
                    <span class="nav-text">
                           <span>Market Blocked</span>
                    </span>
                  </a>
                </li>

              </ul>
            </div>

            <b class="sub-arrow"></b>

          </li>



          <li class="nav-item">

            <a href="#" class="nav-link dropdown-toggle collapsed">
              <i class="nav-icon fa fa-tag"></i>
              <span class="nav-text fadeable">
               <span>All Agencies</span>
              <span class="badge badge-primary py-1 radius-round text-90 mr-2px badge-sm ">5</span>
              </span>

              <b class="caret fa fa-angle-left rt-n90"></b>
            </a>

            <div class="hideable submenu collapse">
              <ul class="submenu-inner">

                <li class="nav-item">

                  <a href="{{route('admin.dashboard')}}" class="nav-link">

                    <span class="nav-text">
                           <span>Profile</span>
                    </span>
                  </a>
                </li>

                <li class="nav-item">
                  <a href="html/page-login.html" class="nav-link">
                    <span class="nav-text">
                           <span>Login</span>
                    </span>
                  </a>
                </li>

                <li class="nav-item">
                  <a href="html/page-pricing.html" class="nav-link">
                    <span class="nav-text">
                           <span>Pricing</span>
                    </span>
                  </a>
                </li>

              </ul>
            </div>

            <b class="sub-arrow"></b>

          </li>

        </ul>

      </div><!-- /.sidebar scroll -->


      <div class="sidebar-section">
        <div class="sidebar-section-item fadeable-bottom">
          <div class="fadeinable">
            <!-- shows this when collapsed -->
            <div class="pos-rel">
              <img alt="Alexa's Photo" src="{{asset('public/backend/')}}/assets/image/avatar/avatar3.jpg" width="42" class="px-1px radius-round mx-2 border-2 brc-default-m2" />
              <span class="bgc-success radius-round border-2 brc-white p-1 position-tr mr-1 mt-2px"></span>
            </div>
          </div>

          <div class="fadeable hideable w-100 bg-transparent shadow-none border-0">
            <!-- shows this when full-width -->
            <div id="sidebar-footer-bg" class="d-flex align-items-center bgc-white shadow-sm mx-2 mt-2px py-2 radius-t-1 border-x-1 border-t-2 brc-primary-m3">
              <div class="d-flex mr-auto py-1">
                <div class="pos-rel">
                  <img alt="Alexa's Photo" src="{{asset('public/backend/')}}/assets/image/avatar/avatar3.jpg" width="42" class="px-1px radius-round mx-2 border-2 brc-default-m2" />
                  <span class="bgc-success radius-round border-2 brc-white p-1 position-tr mr-1 mt-2px"></span>
                </div>

                <div>
                  <span class="text-blue-d1 font-bolder">{{Auth::user()->last_name}}</span>
                  <div class="text-80 text-grey">
                    {{Auth::user()->position}}
                  </div>
                </div>
              </div>

              <a href="#" class="d-style btn btn-outline-primary btn-h-light-primary btn-a-light-primary border-0 p-2 mr-2px ml-4" title="Settings" data-toggle="modal" data-target="#id-ace-settings-modal">
                <i class="fa fa-cog text-150 text-blue-d2 f-n-hover"></i>
              </a>

              {{-- <a href="{{ route('logout') }}" class="d-style btn btn-outline-orange btn-h-light-orange btn-a-light-orange border-0 p-2 mr-1" title="Logout">
                <i class="fa fa-sign-out-alt text-150 text-orange-d2 f-n-hover"></i>
              </a> --}}

              <form method="POST" action="{{ route('logout') }}">
                @csrf
                    <a class="d-style btn btn-outline-orange btn-h-light-orange btn-a-light-orange border-0 p-2 mr-1" title="Logout" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                this.closest('form').submit();">
                         <i class="fa fa-sign-out-alt text-150 text-orange-d2 f-n-hover"></i>
                    </a>
            </form>

            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
