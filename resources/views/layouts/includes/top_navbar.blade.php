{{-- Loader --}}
		{{-- <div id="global-loader">
			<img src="{{asset('assets/images/loader.svg')}}" class="loader-img" alt="">
		</div> --}}

		{{-- Topbar --}}
		<div class="header-main">
			<div class="top-bar">
				<div class="container">
					<div class="row">
						<div class="col-xl-8 col-lg-8 col-sm-4 col-7">
							<div class="top-bar-left d-flex">
								<div class="clearfix">
									<ul class="socials">
										<li>
											<a class="social-icon text-dark" href="#"><i class="fa fa-facebook"></i></a>
										</li>
										<li>
											<a class="social-icon text-dark" href="#"><i class="fa fa-twitter"></i></a>
										</li>
										<li>
											<a class="social-icon text-dark" href="#"><i class="fa fa-linkedin"></i></a>
										</li>
										<li>
											<a class="social-icon text-dark" href="#"><i class="fa fa-google-plus"></i></a>
										</li>
									</ul>
								</div>
								<div class="clearfix">
									<ul class="contact">
										<li class="mr-5 d-lg-none">
											<a href="#" class="callnumber text-dark"><span><i class="fa fa-phone mr-1"></i>: +425 345 8765</span></a>
										</li>
										<li class="select-country mr-5">
											<select class="form-control select2-flag-search" data-placeholder="Select Country">
												<option value="UM">United States of America</option>
												<option value="AF">Afghanistan</option>
												<option value="AL">Albania</option>
												<option value="AD">Andorra</option>
												<option value="AG">Antigua and Barbuda</option>
												<option value="AU">Australia</option>
												<option value="AM">Armenia</option>
												<option value="AO">Angola</option>
												<option value="AR">Argentina</option>
												<option value="AT">Austria</option>
												<option value="AZ">Azerbaijan</option>
												<option value="BA">Bosnia and Herzegovina</option>
												<option value="BB">Barbados</option>
												<option value="BD">Bangladesh</option>
												<option value="BE">Belgium</option>
												<option value="BF">Burkina Faso</option>
												<option value="BG">Bulgaria</option>
												<option value="BH">Bahrain</option>
												<option value="BJ">Benin</option>
												<option value="BN">Brunei</option>
												<option value="BO">Bolivia</option>
												<option value="BT">Bhutan</option>
												<option value="BY">Belarus</option>
												<option value="CD">Congo</option>
												<option value="CA">Canada</option>
												<option value="CF">Central African Republic</option>
												<option value="CI">Cote d'Ivoire</option>
												<option value="CL">Chile</option>
												<option value="CM">Cameroon</option>
												<option value="CN">China</option>
												<option value="CO">Colombia</option>
												<option value="CU">Cuba</option>
												<option value="CV">Cabo Verde</option>
												<option value="CY">Cyprus</option>
												<option value="DJ">Djibouti</option>
												<option value="DK">Denmark</option>
												<option value="DM">Dominica</option>
												<option value="DO">Dominican Republic</option>
												<option value="EC">Ecuador</option>
												<option value="EE">Estonia</option>
												<option value="ER">Eritrea</option>
												<option value="ET">Ethiopia</option>
												<option value="FI">Finland</option>
												<option value="FJ">Fiji</option>
												<option value="FR">France</option>
												<option value="GA">Gabon</option>
												<option value="GD">Grenada</option>
												<option value="GE">Georgia</option>
												<option value="GH">Ghana</option>
												<option value="GH">Ghana</option>
												<option value="HN">Honduras</option>
												<option value="HT">Haiti</option>
												<option value="HU">Hungary</option>
												<option value="ID">Indonesia</option>
												<option value="IE">Ireland</option>
												<option value="IL">Israel</option>
												<option value="IN">India</option>
												<option value="IQ">Iraq</option>
												<option value="IR">Iran</option>
												<option value="IS">Iceland</option>
												<option value="IT">Italy</option>
												<option value="JM">Jamaica</option>
												<option value="JO">Jordan</option>
												<option value="JP">Japan</option>
												<option value="KE">Kenya</option>
												<option value="KG">Kyrgyzstan</option>
												<option value="KI">Kiribati</option>
												<option value="KW">Kuwait</option>
												<option value="KZ">Kazakhstan</option>
												<option value="LA">Laos</option>
												<option value="LB">Lebanons</option>
												<option value="LI">Liechtenstein</option>
												<option value="LR">Liberia</option>
												<option value="LS">Lesotho</option>
												<option value="LT">Lithuania</option>
												<option value="LU">Luxembourg</option>
												<option value="LV">Latvia</option>
												<option value="LY">Libya</option>
												<option value="MA">Morocco</option>
												<option value="MC">Monaco</option>
												<option value="MD">Moldova</option>
												<option value="ME">Montenegro</option>
												<option value="MG">Madagascar</option>
												<option value="MH">Marshall Islands</option>
												<option value="MK">Macedonia (FYROM)</option>
												<option value="ML">Mali</option>
												<option value="MM">Myanmar (formerly Burma)</option>
												<option value="MN">Mongolia</option>
												<option value="MR">Mauritania</option>
												<option value="MT">Malta</option>
												<option value="MV">Maldives</option>
												<option value="MW">Malawi</option>
												<option value="MX">Mexico</option>
												<option value="MZ">Mozambique</option>
												<option value="NA">Namibia</option>
												<option value="NG">Nigeria</option>
												<option value="NO">Norway</option>
												<option value="NP">Nepal</option>
												<option value="NR">Nauru</option>
												<option value="NZ">New Zealand</option>
												<option value="OM">Oman</option>
												<option value="PA">Panama</option>
												<option value="PF">Paraguay</option>
												<option value="PG">Papua New Guinea</option>
												<option value="PH">Philippines</option>
												<option value="PK">Pakistan</option>
												<option value="PL">Poland</option>
												<option value="QA">Qatar</option>
												<option value="RO">Romania</option>
												<option value="RU">Russia</option>
												<option value="RW">Rwanda</option>
												<option value="SA">Saudi Arabia</option>
												<option value="SB">Solomon Islands</option>
												<option value="SC">Seychelles</option>
												<option value="SD">Sudan</option>
												<option value="SE">Sweden</option>
												<option value="SG">Singapore</option>
												<option value="TG">Togo</option>
												<option value="TH">Thailand</option>
												<option value="TJ">Tajikistan</option>
												<option value="TL">Timor-Leste</option>
												<option value="TM">Turkmenistan</option>
												<option value="TN">Tunisia</option>
												<option value="TO">Tonga</option>
												<option value="TR">Turkey</option>
												<option value="TT">Trinidad and Tobago</option>
												<option value="TW">Taiwan</option>
												<option value="UA">Ukraine</option>
												<option value="UG">Uganda</option>
												<option value="UY">Uruguay</option>
												<option value="UZ">Uzbekistan</option>
												<option value="VA">Vatican City (Holy See)</option>
												<option value="VE">Venezuela</option>
												<option value="VN">Vietnam</option>
												<option value="VU">Vanuatu</option>
												<option value="YE">Yemen</option>
												<option value="ZM">Zambia</option>
												<option value="ZW">Zimbabwe</option>
											</select>
										</li>
										<li class="dropdown mr-5">
											<a href="#" class="text-dark" data-toggle="dropdown"><span> Language <i class="fa fa-caret-down text-muted"></i></span> </a>
											<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
												<a href="#" class="dropdown-item" >
													English
												</a>
												<a class="dropdown-item" href="#">
													Arabic
												</a>
												<a class="dropdown-item" href="#">
													German
												</a>
												<a href="#" class="dropdown-item" >
													Greek
												</a>
												<a href="#" class="dropdown-item" >
													Japanese
												</a>
											</div>
										</li>
										<li class="dropdown">
											<a href="#" class="text-dark" data-toggle="dropdown"><span>Currency <i class="fa fa-caret-down text-muted"></i></span></a>
											<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
												<a href="#" class="dropdown-item" >
													USD
												</a>
												<a class="dropdown-item" href="#">
													EUR
												</a>
												<a class="dropdown-item" href="#">
													INR
												</a>
												<a href="#" class="dropdown-item" >
													GBP
												</a>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-lg-4 col-sm-8 col-5">
							<div class="top-bar-right">
								<ul class="custom">

                                    @if (Route::has('login'))
                                        {{-- <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block"> --}}
                                            @auth
                                                @if (Auth::user()->utype==='ADM')
                                                    <li class="dropdown">
                                                        <a href="{{route('admin.dashboard')}}" class="text-dark" data-toggle="dropdown"><i class="fa fa-user mr-1"></i><span> {{Auth::user()->name}}</span></a>
                                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                            <a href="{{route('admin.dashboard')}}" class="dropdown-item" >
                                                                <i class="dropdown-icon icon icon-user"></i> My Profile
                                                            </a>
                                                            <a class="dropdown-item" href="#">
                                                                <i class="dropdown-icon icon icon-speech"></i> Inbox
                                                            </a>
                                                            <a class="dropdown-item" href="#">
                                                                <i class="dropdown-icon icon icon-bell"></i> Notifications
                                                            </a>
                                                            <a href="mydash.html" class="dropdown-item" >
                                                                <i class="dropdown-icon  icon icon-settings"></i> Account Settings
                                                            </a>
                                                            <form method="POST" action="{{ route('logout') }}">
                                                                @csrf
                                                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                                                                onclick="event.preventDefault();
                                                                                this.closest('form').submit();">
                                                                        <i class="dropdown-icon icon icon-power"></i> Log out
                                                                    </a>
                                                            </form>


                                                        </div>
                                                    </li>
                                                @else
                                                    <li class="dropdown">
                                                        <a href="{{route('user.dashboard')}}" class="text-dark" data-toggle="dropdown"><i class="dropdown-icon icon icon-user"></i><span> {{Auth::user()->name}}</span></a>
                                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                            <a href="{{route('user.dashboard')}}" class="dropdown-item" >
                                                                <i class="dropdown-icon icon icon-user"></i> My Profile
                                                            </a>
                                                            <a class="dropdown-item" href="#">
                                                                <i class="dropdown-icon icon icon-speech"></i> Inbox
                                                            </a>
                                                            <a class="dropdown-item" href="#">
                                                                <i class="dropdown-icon icon icon-bell"></i> Notifications
                                                            </a>
                                                            <a href="mydash.html" class="dropdown-item" >
                                                                <i class="dropdown-icon  icon icon-settings"></i> Account Settings
                                                            </a>
                                                            <form method="POST" action="{{ route('logout') }}">
                                                                @csrf
                                                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                                                                onclick="event.preventDefault();
                                                                                this.closest('form').submit();">
                                                                        <i class="dropdown-icon icon icon-power"></i> Log out
                                                                    </a>
                                                            </form>
                                                        </div>
                                                    </li>
                                                @endif



                                            @else
                                                <li>
                                                    <a href="{{ route('login') }}" class="text-dark"><i class="fa fa-sign-in mr-1"></i> <span>Login</span></a>
                                                </li>
                                                @if (Route::has('register'))
                                                    <li>
                                                        <a href="{{ route('register') }}" class="text-dark"><i class="fa fa-user mr-1"></i> <span>Register</span></a>
                                                    </li>
                                                @endif
                                            @endauth
                                        {{-- </div> --}}
                                    @endif
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Duplex Houses Header -->
			<div class="horizontal-header clearfix ">
				<div class="container">
					<a id="horizontal-navtoggle" class="animated-arrow"><span></span></a>
					<span class="smllogo"><img src="{{ asset('public') }}/assets/images/brand/logo.png" width="120" alt=""/></span>
					<a href="tel:245-6325-3256" class="callusbtn"><i class="fa fa-phone" aria-hidden="true"></i></a>
				</div>
			</div>
			<!-- /Duplex Houses Header -->

			<div class="horizontal-main bg-dark-transparent clearfix">
				<div class="horizontal-mainwrapper container clearfix">
					<div class="desktoplogo">
						<a href="{{url('/')}}"><img src="{{ asset('public') }}/assets/images/brand/logo1.png" alt=""></a>
					</div>
					<div class="desktoplogo-1">
						<a href="{{url('/')}}"><img src="{{ asset('public') }}/assets/images/brand/logo1.png" alt=""></a>
					</div>
					<!--Nav-->
					<nav class="horizontalMenu clearfix d-md-flex">
						<ul class="horizontalMenu-list">

                            <li aria-haspopup="true"><a href="{{url('/buy')}}">Buy</a>
								<div class="horizontal-megamenu clearfix">
									<div class="container">
										<div class="megamenu-content">
											<div class="row">
												<ul class="col link-list">
													<li class="title">Search by Property</li>

                                                    <li>
														<a href="page-list.html">RealEstate List</a>
													</li>
													<li>
														<a href="page-list-right.html">RealEstate  Right</a>
													</li>
													<li>
														<a href="page-list-map.html">RealEstate Map list</a>
													</li>
												</ul>
												<ul class="col link-list">
													<li class="title">Search by Location</li>
													<li>
														<a href="ad-list.html">Ad Listing</a>
													</li>
													<li>
														<a href="ad-list-right.html">Ad Listing Right</a>
													</li>
													<li>
														<a href="ad-details.html">Ad Details</a>
													</li>

												</ul>
												<ul class="col link-list">
													<li class="title">Buying Guides</li>
													<li>
														<a href="userprofile.html"> User Profile</a>
													</li>
													<li>
														<a href="mydash.html">My Dashboard</a>
													</li>
													<li>
														<a href="myads.html">Ads</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>

                            <li aria-haspopup="true"><a href="{{url('/rent')}}">Rent</a>
								<div class="horizontal-megamenu clearfix">
									<div class="container">
										<div class="megamenu-content">
											<div class="row">
												<ul class="col link-list">
													<li class="title">Search by Property</li>

                                                    <li>
														<a href="page-list.html">RealEstate List</a>
													</li>
													<li>
														<a href="page-list-right.html">RealEstate  Right</a>
													</li>
													<li>
														<a href="page-list-map.html">RealEstate Map list</a>
													</li>
												</ul>
												<ul class="col link-list">
													<li class="title">Search by Location</li>
													<li>
														<a href="ad-list.html">Ad Listing</a>
													</li>
													<li>
														<a href="ad-list-right.html">Ad Listing Right</a>
													</li>
													<li>
														<a href="ad-details.html">Ad Details</a>
													</li>

												</ul>
												<ul class="col link-list">
													<li class="title">Buying Guides</li>
													<li>
														<a href="userprofile.html"> User Profile</a>
													</li>
													<li>
														<a href="mydash.html">My Dashboard</a>
													</li>
													<li>
														<a href="myads.html">Ads</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>

                            <li aria-haspopup="true"><a href="#">New Developments</a>
								<div class="horizontal-megamenu clearfix">
									<div class="container">
										<div class="megamenu-content">
											<div class="row">
												<ul class="col link-list">
													<li class="title">Search by Property</li>

                                                    <li>
														<a href="page-list.html">RealEstate List</a>
													</li>
													<li>
														<a href="page-list-right.html">RealEstate  Right</a>
													</li>
													<li>
														<a href="page-list-map.html">RealEstate Map list</a>
													</li>
												</ul>
												<ul class="col link-list">
													<li class="title">Search by Location</li>
													<li>
														<a href="ad-list.html">Ad Listing</a>
													</li>
													<li>
														<a href="ad-list-right.html">Ad Listing Right</a>
													</li>
													<li>
														<a href="ad-details.html">Ad Details</a>
													</li>

												</ul>
												<ul class="col link-list">
													<li class="title">Buying Guides</li>
													<li>
														<a href="userprofile.html"> User Profile</a>
													</li>
													<li>
														<a href="mydash.html">My Dashboard</a>
													</li>
													<li>
														<a href="myads.html">Ads</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>

                            <li aria-haspopup="true"><a href="#">Condo</a>
								<div class="horizontal-megamenu clearfix">
									<div class="container">
										<div class="megamenu-content">
											<div class="row">
												<ul class="col link-list">
													<li class="title">Search by Property</li>

                                                    <li>
														<a href="page-list.html">RealEstate List</a>
													</li>
													<li>
														<a href="page-list-right.html">RealEstate  Right</a>
													</li>
													<li>
														<a href="page-list-map.html">RealEstate Map list</a>
													</li>
												</ul>
												<ul class="col link-list">
													<li class="title">Search by Location</li>
													<li>
														<a href="ad-list.html">Ad Listing</a>
													</li>
													<li>
														<a href="ad-list-right.html">Ad Listing Right</a>
													</li>
													<li>
														<a href="ad-details.html">Ad Details</a>
													</li>

												</ul>
												<ul class="col link-list">
													<li class="title">Buying Guides</li>
													<li>
														<a href="userprofile.html"> User Profile</a>
													</li>
													<li>
														<a href="mydash.html">My Dashboard</a>
													</li>
													<li>
														<a href="myads.html">Ads</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>

                            <li aria-haspopup="true"><a href="#">Boreys</a>
								<div class="horizontal-megamenu clearfix">
									<div class="container">
										<div class="megamenu-content">
											<div class="row">
												<ul class="col link-list">
													<li class="title">Search by Property</li>

                                                    <li>
														<a href="page-list.html">RealEstate List</a>
													</li>
													<li>
														<a href="page-list-right.html">RealEstate  Right</a>
													</li>
													<li>
														<a href="page-list-map.html">RealEstate Map list</a>
													</li>
												</ul>
												<ul class="col link-list">
													<li class="title">Search by Location</li>
													<li>
														<a href="ad-list.html">Ad Listing</a>
													</li>
													<li>
														<a href="ad-list-right.html">Ad Listing Right</a>
													</li>
													<li>
														<a href="ad-details.html">Ad Details</a>
													</li>

												</ul>
												<ul class="col link-list">
													<li class="title">Buying Guides</li>
													<li>
														<a href="userprofile.html"> User Profile</a>
													</li>
													<li>
														<a href="mydash.html">My Dashboard</a>
													</li>
													<li>
														<a href="myads.html">Ads</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>

							<li aria-haspopup="true"><a href="about.html">News</a></li>

							<li aria-haspopup="true"><a href="#">Land <span class="fa fa-caret-down m-0"></span></a>
								<ul class="sub-menu">
									<li aria-haspopup="true"><a href="#">Blog Grid <i class="fa fa-angle-right float-right mt-1 d-none d-lg-block"></i></a>
										 <ul class="sub-menu">
											<li aria-haspopup="true"><a href="blog-grid.html">Blog Grid Left</a></li>
											<li aria-haspopup="true"><a href="blog-grid-right.html">Blog Grid Right</a></li>
											<li aria-haspopup="true"><a href="blog-grid-center.html">Blog Grid Center</a></li>
										</ul>
									</li>
									<li aria-haspopup="true"><a href="#">Blog List <i class="fa fa-angle-right float-right mt-1 d-none d-lg-block"></i></a>
										 <ul class="sub-menu">
											<li aria-haspopup="true"><a href="blog-list.html">Blog List Left</a></li>
											<li aria-haspopup="true"><a href="blog-list-right.html">Blog List Right</a></li>
											<li aria-haspopup="true"><a href="blog-list-center.html">Blog List Center</a></li>
										</ul>
									</li>
									<li aria-haspopup="true"><a href="#">Blog Details <i class="fa fa-angle-right float-right mt-1 d-none d-lg-block"></i></a>
										<ul class="sub-menu">
											<li aria-haspopup="true"><a href="blog-details.html">Blog Details Left</a></li>
											<li aria-haspopup="true"><a href="blog-details-right.html">Blog Details Right</a></li>
											<li aria-haspopup="true"><a href="blog-details-center.html">Blog Details Center</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li aria-haspopup="true"><a href="#">More <span class="fa fa-caret-down m-0"></span></a>
								<ul class="sub-menu">
									<li aria-haspopup="true"><a href="col-left.html">RealEstate Left</a></li>
									<li aria-haspopup="true"><a href="col-right.html">RealEstate  Right </a></li>
								</ul>
							</li>
                            <li aria-haspopup="true" class="d-lg-none mt-5 pb-5 mt-lg-0">
								<span><a class="btn btn-secondary" href="ad-posts.html">Post Property Ad</a></span>
							</li>
						</ul>
						<ul class="mb-0">
							<li aria-haspopup="true" class="mt-3 d-none d-lg-block top-postbtn">
								<span><a class="btn btn-secondary ad-post " href="ad-posts.html">Post Property Ad</a></span>
							</li>
						</ul>
					</nav>
					<!--Nav-->
				</div>
			</div>
		</div>
