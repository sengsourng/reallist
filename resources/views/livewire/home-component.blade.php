    	{{-- Sliders Section --}}
		<div>
			<div class=" cover-image sptb-4 sptb-tab bg-background2" data-image-src="{{ asset('public') }}/assets/images/banners/banner1.jpg">
				<div class="header-text mb-0">
					<div class="container">
						<div class="text-center text-white mb-6">
							<a href="" class="typewrite" data-period="2000" data-type='[ "Find Your Best Property" ]'>
								<span class="wrap"></span>
							</a>
							<p>It is a long established fact that a reader will be distracted by the readable.</p>
						</div>
						<div class="row">
							<div class="col-xl-10 col-lg-12 col-md-12 d-block mx-auto">
								<div class="search-background bg-transparent">
									<div class="form row no-gutters ">
										<div class="form-group  col-xl-4 col-lg-3 col-md-12 mb-0 bg-white ">
											<input type="text" class="form-control input-lg br-tr-md-0 br-br-md-0" id="text4" placeholder="Find Your Property">
										</div>
										<div class="form-group  col-xl-3 col-lg-3 col-md-12 mb-0 bg-white">
											<input type="text" class="form-control input-lg br-md-0" id="text5" placeholder="Enter Location">
											<span><i class="fa fa-map-marker location-gps mr-1"></i> </span>
										</div>
										<div class="form-group col-xl-3 col-lg-3 col-md-12 select2-lg  mb-0 bg-white">
											<select class="form-control select2-show-search  border-bottom-0" data-placeholder="Select Category">
												<optgroup label="Categories">
													<option>Select</option>
													<option value="2">Apartments</option>
													<option value="3">Houses & Villas</option>
													<option value="4">Builder Floors</option>
													<option value="5">Lands</option>
													<option value="6">Plots</option>
													<option value="7">Shops</option>
													<option value="8">Offices</option>
													<option value="9">Farm Houses</option>
												</optgroup>
											</select>
										</div>
										<div class="col-xl-2 col-lg-3 col-md-12 mb-0">
											<a href="#" class="btn btn-lg btn-block btn-primary br-tl-md-0 br-bl-md-0">Search Here</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- /header-text -->
			</div>
		</div>
		{{-- Sliders Section --}}

		{{--  Categories --}}
		<section class="sptb bg-white">
			<div class="container">
				<div class="section-title center-block text-center">
					<h2>Categories</h2>
					<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
				</div>
				<div class="row">
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-6">
						<div class="card bg-card-light">
							<div class="card-body">
								<div class="cat-item text-center">
									<a href="col-left.html"></a>
									<div class="cat-img">
										<img src="{{ asset('public') }}/assets/images/svgs/realestate/balcony.svg" alt="img">
									</div>
									<div class="cat-desc">
										<h5 class="mb-1">2BHK Homes</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-6">
						<div class="card bg-card-light">
							<div class="card-body">
								<div class="cat-item text-center">
									<a href="col-left.html"></a>
									<div class="cat-img">
										<img src="{{ asset('public') }}/assets/images/svgs/realestate/appartment.svg" alt="img">
									</div>
									<div class="cat-desc">
										<h5 class="mb-1">Deluxe Flats</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-6">
						<div class="card bg-card-light">
							<div class="card-body">
								<div class="cat-item text-center">
									<a href="col-left.html"></a>
									<div class="cat-img">
										<img src="{{ asset('public') }}/assets/images/svgs/realestate/beach-house.svg" alt="img">
									</div>
									<div class="cat-desc">
										<h5 class="mb-1">Offices</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-6">
						<div class="card bg-card-light">
							<div class="card-body">
								<div class="cat-item text-center">
									<a href="col-left.html"></a>
									<div class="cat-img text-shadow1">
										<img src="{{ asset('public') }}/assets/images/svgs/realestate/building.svg" alt="img">
									</div>
									<div class="cat-desc">
										<h5 class="mb-1">Apartments</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-6">
						<div class="card bg-card-light">
							<div class="card-body">
								<div class="cat-item text-center">
									<a href="col-left.html"></a>
									<div class="cat-img">
										<img src="{{ asset('public') }}/assets/images/svgs/realestate/cabin.svg" alt="img">
									</div>
									<div class="cat-desc">
										<h5 class="mb-1">Budget House</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-6">
						<div class="card bg-card-light">
							<div class="card-body">
								<div class="cat-item text-center">
									<a href="col-left.html"></a>
									<div class="cat-img">
										<img src="{{ asset('public') }}/assets/images/svgs/realestate/farm.svg" alt="img">
									</div>
									<div class="cat-desc">
										<h5 class="mb-1">Farm House</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-6">
						<div class="card bg-card-light mb-xl-0">
							<div class="card-body">
								<div class="cat-item text-center">
									<a href="col-left.html"></a>
									<div class="cat-img">
										<img src="{{ asset('public') }}/assets/images/svgs/realestate/house.svg" alt="img">
									</div>
									<div class="cat-desc">
										<h5 class="mb-1">2BHK House</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-6">
						<div class="card bg-card-light mb-xl-0">
							<div class="card-body">
								<div class="cat-item text-center">
									<a href="col-left.html"></a>
									<div class="cat-img">
										<img src="{{ asset('public') }}/assets/images/svgs/realestate/mansion.svg" alt="img">
									</div>
									<div class="cat-desc">
										<h5 class="mb-1">Duplex House</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-6">
						<div class="card bg-card-light mb-md-0">
							<div class="card-body">
								<div class="cat-item text-center">
									<a href="col-left.html"></a>
									<div class="cat-img">
										<img src="{{ asset('public') }}/assets/images/svgs/realestate/appartment.svg" alt="img">
									</div>
									<div class="cat-desc">
										<h5 class="mb-1">3BHK Flats</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-6">
						<div class="card bg-card-light mb-md-0">
							<div class="card-body">
								<div class="cat-item text-center">
									<a href="col-left.html"></a>
									<div class="cat-img">
										<img src="{{ asset('public') }}/assets/images/svgs/realestate/cabin.svg" alt="img">
									</div>
									<div class="cat-desc">
										<h5 class="mb-1">Single Houses</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-6">
						<div class="card bg-card-light mb-sm-0">
							<div class="card-body">
								<div class="cat-item text-center">
									<a href="col-left.html"></a>
									<div class="cat-img">
										<img src="{{ asset('public') }}/assets/images/svgs/realestate/house.svg" alt="img">
									</div>
									<div class="cat-desc">
										<h5 class="mb-1">Luxury Rooms</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-6">
						<div class="card bg-card-light mb-0">
							<div class="card-body">
								<div class="cat-item text-center">
									<a href="col-left.html"></a>
									<div class="cat-img">
										<img src="{{ asset('public') }}/assets/images/svgs/realestate/beach-house.svg" alt="img">
									</div>
									<div class="cat-desc">
										<h5 class="mb-1">Offices</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		{{-- Categories --}}

		{{-- Latest Ads --}}
		<section class="sptb">
			<div class="container">
				<div class="section-title center-block text-center">
					<h2>Latest Properties</h2>
					<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
				</div>
				<div id="myCarousel1" class="owl-carousel owl-carousel-icons2">
					<div class="item">
						<div class="card mb-0">
							<div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
							<div class="item-card2-img">
								<a href="col-left.html"></a>
								<img src="{{ asset('public') }}/assets/images/products/products/f1.jpg" alt="img" class="cover-image">
								<div class="tag-text">
									<span class="bg-danger tag-option">For Sale </span>
									<span class="bg-pink tag-option">Open</span>
								</div>
							</div>
							<div class="item-card2-icons">
								<a href="col-left.html" class="item-card2-icons-l bg-primary"> <i class="fa fa-home"></i></a>
								<a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
							</div>
							<div class="card-body">
								<div class="item-card2">
									<div class="item-card2-text">
										<a href="col-left.html" class="text-dark"><h4 class="">Deluxe Houses</h4></a>
										<p class="mb-2"><i class="fa fa-map-marker text-danger mr-1"></i> Preston Street Wichita , USA </p>
										<h5 class="font-weight-bold mb-3">$89,005 <span class="fs-12  font-weight-normal">Per Month</span></h5>
									</div>
									<ul class="item-card2-list">
										<li><a href="#"><i class="fa fa-arrows-alt text-muted mr-1"></i> 256 Sqft</a></li>
										<li><a href="#" class="icons"><i class="fa fa-bed text-muted mr-1"></i> 3 Beds</a></li>
										<li><a href="#" class="icons"><i class="fa fa-bath text-muted mr-1"></i> 2 Bath</a></li>
										<li><a href="#" class="icons"><i class="fa fa-car text-muted mr-1"></i> 1 Car</a></li>
									</ul>
								</div>
							</div>
							<div class="card-footer">
								<div class="footerimg d-flex mt-0 mb-0">
									<div class="d-flex footerimg-l mb-0">
										<img src="{{ asset('public') }}/assets/images/faces/male/18.jpg" alt="image" class="avatar brround  mr-2">
										<h5 class="time-title text-muted p-0 leading-normal mt-1 mb-0">Wendy	Peake<i class="si si-check text-success fs-12 ml-1" data-toggle="tooltip" data-placement="top" title="verified"></i></h5>
									</div>
									<div class="mt-2 footerimg-r ml-auto">
										<small class="text-muted">1 day ago</small>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="card mb-0">
							<div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
							<div class="item-card2-img">
								<a href="col-left.html"></a>
								<img src="{{ asset('public') }}/assets/images/products/products/h4.jpg" alt="img" class="cover-image">
								<div class="tag-text"><span class="bg-danger tag-option">For Rent </span></div>
							</div>
							<div class="item-card2-icons">
								<a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-home"></i></a>
								<a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
							</div>
							<div class="card-body">
								<div class="item-card2">
									<div class="item-card2-text">
										<a href="col-left.html" class="text-dark"><h4 class="">2BK Houses</h4></a>
										<p class="mb-2"><i class="fa fa-map-marker text-danger mr-1"></i> Preston Street Wichita , USA </p>
										<h5 class="font-weight-bold mb-3">$12,890 <span class="fs-12  font-weight-normal">Per Month</span></h5>
									</div>
									<ul class="item-card2-list">
										<li><a href="#"><i class="fa fa-arrows-alt text-muted mr-1"></i> 150 Sqft</a></li>
										<li><a href="#" class="icons"><i class="fa fa-bed text-muted mr-1"></i> 2 Beds</a></li>
										<li><a href="#" class="icons"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a></li>
										<li><a href="#" class="icons"><i class="fa fa-car text-muted mr-1"></i> 1 Car</a></li>
									</ul>
								</div>
							</div>
							<div class="card-footer">
								<div class="footerimg d-flex mt-0 mb-0">
									<div class="d-flex footerimg-l mb-0">
										<img src="{{ asset('public') }}/assets/images/faces/female/12.jpg" alt="image" class="avatar brround  mr-2">
										<h5 class="time-title text-muted p-0 leading-normal mt-1 mb-0">Ryan Lyman<i class="si si-check text-success fs-12 ml-1" data-toggle="tooltip" data-placement="top" title="verified"></i></h5>
									</div>
									<div class="mt-2 footerimg-r ml-auto">
										<small class="text-muted">55 mins ago</small>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="card mb-0">
							<div class="item-card2-img">
								<a href="col-left.html"></a>
								<img src="{{ asset('public') }}/assets/images/products/products/b1.jpg" alt="img" class="cover-image">
								<div class="tag-text">
									<span class="bg-danger tag-option">For Rent </span>
									<span class="bg-pink tag-option">Hot</span>
								</div>
							</div>
							<div class="item-card2-icons">
								<a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-home"></i></a>
								<a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
							</div>
							<div class="card-body">
								<div class="item-card2">
									<div class="item-card2-text">
										<a href="col-left.html" class="text-dark"><h4 class="">Office Rooms</h4></a>
										<p class="mb-2"><i class="fa fa-map-marker text-danger mr-1"></i> Preston Street Wichita , USA </p>
										<h5 class="font-weight-bold mb-3">$25,784 <span class="fs-12  font-weight-normal">Per Month</span></h5>
									</div>
									<ul class="item-card2-list">
										<li><a href="#"><i class="fa fa-arrows-alt text-muted mr-1"></i> 256 Sqft</a></li>
										<li><a href="#" class="icons"><i class="fa fa-bed text-muted mr-1"></i> 8 Beds</a></li>
										<li><a href="#" class="icons"><i class="fa fa-bath text-muted mr-1"></i> 4 Bath</a></li>
										<li><a href="#" class="icons"><i class="fa fa-car text-muted mr-1"></i> 4 Car</a></li>
									</ul>
								</div>
							</div>
							<div class="card-footer">
								<div class="footerimg d-flex mt-0 mb-0">
									<div class="d-flex footerimg-l mb-0">
										<img src="{{ asset('public') }}/assets/images/faces/male/8.jpg" alt="image" class="avatar brround  mr-2">
										<h5 class="time-title text-muted p-0 leading-normal mt-1 mb-0">Joan Hunter<i class="si si-check text-success fs-12 ml-1" data-toggle="tooltip" data-placement="top" title="verified"></i></h5>
									</div>
									<div class="mt-2 footerimg-r ml-auto">
										<small class="text-muted">2 day ago</small>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="card mb-0">
							<div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
							<div class="item-card2-img">
								<a href="col-left.html"></a>
								<img src="{{ asset('public') }}/assets/images/products/products/v1.jpg" alt="img" class="cover-image">
								<div class="tag-text"><span class="bg-danger tag-option">For Sale </span></div>
							</div>
							<div class="item-card2-icons">
								<a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-home"></i></a>
								<a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
							</div>
							<div class="card-body">
								<div class="item-card2">
									<div class="item-card2-text">
										<a href="col-left.html" class="text-dark"><h4 class="">Apartments</h4></a>
										<p class="mb-2"><i class="fa fa-map-marker text-danger mr-1"></i> Preston Street Wichita , USA </p>
										<h5 class="font-weight-bold mb-3">$89,005 <span class="fs-12  font-weight-normal">Per Month</span></h5>
									</div>
									<ul class="item-card2-list">
										<li><a href="#"><i class="fa fa-arrows-alt text-muted mr-1"></i> 700 Sqft</a></li>
										<li><a href="#" class="icons"><i class="fa fa-bed text-muted mr-1"></i> 20 Beds</a></li>
										<li><a href="#" class="icons"><i class="fa fa-bath text-muted mr-1"></i> 10 Bath</a></li>
										<li><a href="#" class="icons"><i class="fa fa-car text-muted mr-1"></i> 10 Car</a></li>
									</ul>
								</div>
							</div>
							<div class="card-footer">
								<div class="footerimg d-flex mt-0 mb-0">
									<div class="d-flex footerimg-l mb-0">
										<img src="{{ asset('public') }}/assets/images/faces/female/19.jpg" alt="image" class="avatar brround  mr-2">
										<h5 class="time-title text-muted p-0 leading-normal mt-1 mb-0">Elizabeth<i class="si si-check text-success fs-12 ml-1" data-toggle="tooltip" data-placement="top" title="verified"></i></h5>
									</div>
									<div class="mt-2 footerimg-r ml-auto">
										<small class="text-muted">50 mins ago</small>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item sold-out">
						<div class="ribbon sold-ribbon ribbon-top-left text-danger"><span class="bg-danger">Sold Out</span></div>
						<div class="card mb-0">
							<div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
							<div class="item-card2-img">
								<a href="col-left.html"></a>
								<img src="{{ asset('public') }}/assets/images/products/products/f3.jpg" alt="img" class="cover-image">
								<div class="tag-text">
									<span class="bg-danger tag-option">For Sale </span>
									<span class="bg-pink tag-option">New</span>
								</div>
							</div>
							<div class="item-card2-icons">
								<a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-home"></i></a>
								<a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
							</div>
							<div class="card-body">
								<div class="item-card2">
									<div class="item-card2-text">
										<a href="col-left.html" class="text-dark"><h4 class="">Duplex House</h4></a>
										<p class="mb-2"><i class="fa fa-map-marker text-danger mr-1"></i> Preston Street Wichita , USA </p>
										<h5 class="font-weight-bold mb-3">$23,789 <span class="fs-12  font-weight-normal">Per Month</span></h5>
									</div>
									<ul class="item-card2-list">
										<li><a href="#"><i class="fa fa-arrows-alt text-muted mr-1"></i> 300 Sqft</a></li>
										<li><a href="#" class="icons"><i class="fa fa-bed text-muted mr-1"></i> 4 Beds</a></li>
										<li><a href="#" class="icons"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a></li>
										<li><a href="#" class="icons"><i class="fa fa-car text-muted mr-1"></i> 1 Car</a></li>
									</ul>
								</div>
							</div>
							<div class="card-footer">
								<div class="footerimg d-flex mt-0 mb-0">
									<div class="d-flex footerimg-l mb-0">
										<img src="{{ asset('public') }}/assets/images/faces/female/18.jpg" alt="image" class="avatar brround  mr-2">
										<h5 class="time-title text-muted p-0 leading-normal mt-1 mb-0">Boris Nash<i class="si si-check text-success fs-12 ml-1" data-toggle="tooltip" data-placement="top" title="verified"></i></h5>
									</div>
									<div class="mt-2 footerimg-r ml-auto">
										<small class="text-muted">12 mins ago</small>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		{{-- Latest Ads --}}

		{{-- post section --}}
		<section>
			<div class="cover-image sptb bg-background-color" data-image-src="{{ asset('public') }}/assets/images/banners/banner4.jpg">
				<div class="content-text mb-0">
					<div class="content-text mb-0">
						<div class="container">
							<div class="text-center text-white ">
								<h1 class="mb-2">Subscribe</h1>
								<p class="fs-16">It is a long established fact that a reader will be distracted by the readable.</p>
								<div class="row">
									<div class="col-lg-8 mx-auto d-block">
										<div class="mt-5">
											<div class="input-group sub-input mt-1">
												<input type="text" class="form-control input-lg " placeholder="Enter your Email">
												<div class="input-group-append ">
													<button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3">
														Subscribe
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		{{-- /post section --}}

		{{-- Blogs --}}
		<section class="sptb">
			<div class="container">
				<div class="section-title center-block text-center">
					<h2>What Makes Us The Preferred Choice ?</h2>
					<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-6 col-sm-6">
						<div class="">
							<div class="mb-lg-0 mb-5">
								<div class="service-card text-center">
									<div class="bg-white icon-bg  icon-service text-purple about">
										<i class="fe fe-pocket text-primary"></i>
									</div>
									<div class="servic-data mt-3">
										<h4 class="font-weight-semibold mb-2">Buyers Trust Us</h4>
										<p class="text-muted mb-0">Nam libero tempore, cum soluta nobis est eligendi cumque facere possimus</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6">
						<div class="">
							<div class="mb-sm-0 mb-5">
								<div class="service-card text-center">
									<div class="bg-white icon-bg  icon-service text-purple about">
										<i class="fe fe-thumbs-up text-secondary"></i>
									</div>
									<div class="servic-data mt-3">
										<h4 class="font-weight-semibold mb-2">Seller Prefer Us</h4>
										<p class="text-muted mb-0">Nam libero tempore, cum soluta nobis est eligendi cumque facere possimus</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6">
						<div class="">
							<div class="mb-sm-0 mb-5">
								<div class="service-card text-center">
									<div class="bg-white icon-bg  icon-service text-purple about">
										<i class="fe fe-file-text text-success"></i>
									</div>
									<div class="servic-data mt-3">
										<h4 class="font-weight-semibold mb-2">Maximum Choices</h4>
										<p class="text-muted mb-0">Nam libero tempore, cum soluta nobis est eligendi cumque facere possimus</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6">
						<div class="">
							<div class="">
								<div class="service-card text-center">
									<div class="bg-white icon-bg  icon-service text-purple about">
										<i class="fe fe-users text-info"></i>
									</div>
									<div class="servic-data mt-3">
										<h4 class="font-weight-semibold mb-2">Expert Guidance</h4>
										<p class="text-muted mb-0">Nam libero tempore, cum soluta nobis est eligendi cumque facere possimus</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		{{-- Blogs --}}

		{{-- Testimonials --}}
		<section class="sptb bg-white">
			<div class="container">
				<div class="section-title center-block text-center">
					<h2 class=" position-relative">Testimonials</h2>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="">
							<div id="myCarousel" class="owl-carousel testimonial-owl-carousel">
								<div class="item text-center">
									<div class="card-body p-0">
										<div class="row">
											<div class="col-xl-8 col-md-12 d-block mx-auto">
												<div class="">
													<div class="testimonia-img mx-auto mb-3">
														<img src="{{ asset('public') }}/assets/images/faces/female/11.jpg" class="img-thumbnail rounded-circle alt=" alt="">
													</div>
													<p>
														<i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in cillum dolore eu fugiat nulla pariatur.
													</p>
													<div class="testimonia-data">
														<h4 class="fs-20 mb-1">Heather Bell</h4>
														<div class="rating-stars">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value"  value="3">
															<div class="rating-stars-container mb-3">
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
															</div>
														</div>
														<a href="testimonial.html" class="btn btn-primary btn-lg">View all Testimonials</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="item text-center">
									<div class="card-body p-0">
										<div class="row">
											<div class="col-xl-8 col-md-12 d-block mx-auto">
												<div class="">
													<div class="testimonia-img mx-auto mb-3">
														<img src="{{ asset('public') }}/assets/images/faces/male/42.jpg" class="img-thumbnail rounded-circle alt=" alt="">
													</div>
													<p><i class="fa fa-quote-left"></i> Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Nor again is there anyone who loves or pursues obtain pain of itself, because laboriosam ex ea commodi consequatur. </p>
													<div class="testimonia-data">
														<h4 class="fs-20 mb-1">David Blake</h4>
														<div class="rating-stars">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value"  value="3">
															<div class="rating-stars-container mb-3">
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<a href="testimonial.html" class="btn btn-primary btn-lg">View all Testimonials</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="item text-center">
									<div class="card-body p-0">
										<div class="row">
											<div class="col-xl-8 col-md-12 d-block mx-auto">
												<div class="">
													<div class="testimonia-img mx-auto mb-3">
														<img src="{{ asset('public') }}/assets/images/faces/female/20.jpg" class="img-thumbnail rounded-circle alt=" alt="">
													</div>
													<p><i class="fa fa-quote-left"></i> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum exercitation ullamco laboris nisi.</p>
													<div class="testimonia-data">
														<h4 class="fs-20 mb-1">Sophie Carr</h4>
														<div class="rating-stars mb-3">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value"  value="3">
															<div class="rating-stars-container">
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
																<div class="rating-star sm">
																	<i class="fa fa-star"></i>
																</div>
															</div>
														</div>
														<a href="testimonial.html" class="btn btn-primary btn-lg">View all Testimonials</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		{{-- /Testimonials --}}

		{{-- News --}}
		<section class="sptb">
			<div class="container">
				<div class="section-title center-block text-center">
					<h2>Latest News</h2>
					<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
				</div>
				<div class="row">
					<div class="col-lg-4 col-md-12 col-xl-4">
						<div class="card mb-lg-0">
							<div class="item7-card-img">
								<a href="blog-details.html"></a>
								<img src="{{ asset('public') }}/assets/images/products/products/ed1.jpg" alt="img" class="cover-image">
								<div class="item7-card-text">
									<span class="badge badge-info">Farm House</span>
								</div>
							</div>
							<div class="card-body p-4">
								<div class="item7-card-desc d-flex mb-2">
									<a href="#" class="text-muted"><i class="fa fa-calendar-o text-muted mr-2"></i>July-03-2019</a>
									<div class="ml-auto">
										<a href="#" class="text-muted"><i class="fa fa-comment-o text-muted mr-2"></i>4 Comments</a>
									</div>
								</div>
								<a href="blog-details.html" class="text-dark"><h4 class="fs-20">Luxury Flat For Rent</h4></a>
								<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip commodo consequat </p>
								<div class="d-flex align-items-center pt-2 mt-auto">
									<img src="{{ asset('public') }}/assets/images/faces/male/5.jpg" class="avatar brround avatar-md mr-3" alt="avatar-img">
									<div>
										<a href="profile.html" class="text-default">Joanne Nash</a>
										<small class="d-block text-muted">1 day ago</small>
									</div>
									<div class="ml-auto text-muted">
										<a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fe fe-heart mr-1"></i></a>
										<a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fa fa-thumbs-o-up"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-12 col-xl-4">
						<div class="card mb-lg-0">
							<div class="item7-card-img">
								<a href="blog-details.html"></a>
								<img src="{{ asset('public') }}/assets/images/products/products/j2.jpg" alt="img" class="cover-image">
								<div class="item7-card-text">
									<span class="badge badge-primary">Luxury Room</span>
								</div>
							</div>
							<div class="card-body p-4">
								<div class="item7-card-desc d-flex mb-2">
									<a href="#" class="text-muted"><i class="fa fa-calendar-o text-muted mr-2"></i>June-03-2019</a>
									<div class="ml-auto">
										<a href="#" class="text-muted"><i class="fa fa-comment-o text-muted mr-2"></i>2 Comments</a>
									</div>
								</div>
								<a href="blog-details.html" class="text-dark"><h4 class="fs-20">Deluxe Flat For Sale</h4></a>
								<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip commodo consequat </p>
								<div class="d-flex align-items-center pt-2 mt-auto">
									<img src="{{ asset('public') }}/assets/images/faces/male/7.jpg" class="avatar brround avatar-md mr-3" alt="avatar-img">
									<div>
										<a href="profile.html" class="text-default">Tanner Mallari</a>
										<small class="d-block text-muted">2 days ago</small>
									</div>
									<div class="ml-auto text-muted">
										<a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fe fe-heart mr-1"></i></a>
										<a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fa fa-thumbs-o-up"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-12 col-xl-4">
						<div class="card mb-0">
							<div class="item7-card-img">
								<a href="blog-details.html"></a>
								<img src="{{ asset('public') }}/assets/images/products/products/ed2.jpg" alt="img" class="cover-image">
								<div class="item7-card-text">
									<span class="badge badge-success">2BHK Flat</span>
								</div>
							</div>
							<div class="card-body p-4">
								<div class="item7-card-desc d-flex mb-2">
									<a href="#" class="text-muted"><i class="fa fa-calendar-o text-muted mr-2"></i>June-19-2019</a>
									<div class="ml-auto">
										<a href="#" class="text-muted"><i class="fa fa-comment-o text-muted mr-2"></i>8 Comments</a>
									</div>
								</div>
								<a href="blog-details.html" class="text-dark"><h4 class="fs-20">Luxury Home  For Sale</h4></a>
								<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip commodo consequat </p>
								<div class="d-flex align-items-center pt-2 mt-auto">
									<img src="{{ asset('public') }}/assets/images/faces/female/15.jpg" class="avatar brround avatar-md mr-3" alt="avatar-img">
									<div>
										<a href="profile.html" class="text-default">Aracely Bashore</a>
										<small class="d-block text-muted">5 days ago</small>
									</div>
									<div class="ml-auto text-muted">
										<a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fe fe-heart mr-1"></i></a>
										<a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fa fa-thumbs-o-up"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		{{-- News --}}

		{{-- Download  --}}
		<section class="sptb bg-white">
			<div class="container">
				<div class="section-title center-block text-center">
					<h2>Download</h2>
					<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
				</div>
                <div class="row">
					<div class="col-md-12">
						<div class="text-center text-wrap">
							<div class="btn-list">
								<a href="#" class="btn btn-primary btn-lg mb-sm-0"><i class="fa fa-apple fa-1x mr-2"></i> App Store</a>
								<a href="#" class="btn btn-secondary btn-lg mb-sm-0"><i class="fa fa-android fa-1x mr-2"></i> Google Play</a>
								<a href="#" class="btn btn-info btn-lg mb-0"><i class="fa fa-windows fa-1x mr-2"></i> Windows</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		{{-- Download --}}


