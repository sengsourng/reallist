
    {{-- <div class="page-header">
      <h1 class="page-title text-primary-d2 text-140">
        Simple static tables
      </h1>
    </div> --}}

    <div class="row mt-3">
        <div class="col-12">
          <div class="card dcard">
            <div class="card-body px-1 px-md-3">

              <form autocomplete="off">
                <div class="d-flex justify-content-between flex-column flex-sm-row mb-3 px-2 px-sm-0">
                  <h3 class="text-125 pl-1 mb-3 mb-sm-0 text-secondary-d4">
                    List all User
                  </h3>

                  <div class="pos-rel ml-sm-auto mr-sm-2 order-last order-sm-0">
                    <i class="fa fa-search position-lc ml-25 text-primary-m1"></i>
                    <input type="text" wire:model="search" class="form-control w-100 pl-45 radius-1 brc-primary-m4" placeholder="Search ...">
                  </div>

                   {{-- @if($isModalOpen) --}}
                        @include('livewire.admin.users.create')
                        @include('livewire.admin.users.update')
                    {{-- @endif --}}


                  <div class="mb-2 mb-sm-0">
                    <button wire:click="create()" data-toggle="modal" data-target="#openModal" type="button" class="btn btn-blue px-3 d-block w-100 text-95 radius-round border-2 brc-black-tp10" >
                      <i class="fa fa-plus mr-1"></i>
                      Add <span class="d-sm-none d-md-inline">New</span>
                    </button>
                  </div>
                </div>

                <table id="simple-table" class="mb-0 table table-borderless table-bordered-x brc-secondary-l3 text-dark-m2 radius-1 overflow-hidden">
                  <thead class="text-dark-tp3 bgc-grey-l4 text-90 border-b-1 brc-transparent">
                    <tr>
                      <th class="text-center pr-0">
                        <label class="py-0">
                          <input type="checkbox" class="align-bottom mb-n1 border-2 text-dark-m3">
                        </label>
                      </th>

                      <th>
                        <i class="fa fa-camera" data-toggle="tooltip" title="" data-original-title="fa fa-camera" style="font-size:1.25em;"></i>
                      </th>

                      <th>
                        Username
                      </th>

                      <th class="d-none d-sm-table-cell">
                        Full Name
                      </th>
                      <th class="d-none d-sm-table-cell">
                        Contact
                      </th>

                      <th class="d-none d-sm-table-cell">
                        Created at
                      </th>

                      <th></th>
                      <th></th>
                    </tr>
                  </thead>

                  <tbody class="mt-1">


                @foreach ($users as $user)
                  <tr class="bgc-h-yellow-l4 d-style">
                      <td class="text-center pr-0 pos-rel">
                            <div class="position-tl h-100 ml-n1px border-l-4 brc-orange-m1 v-hover">
                            <!-- border shown on hover -->
                            </div>
                            <div class="position-tl h-100 ml-n1px border-l-4 brc-success-m1 v-active">
                            <!-- border shown when row is selected -->
                            </div>

                            <label class="align-middle" style="padding-top: 15px; ">
                                <input type="checkbox" class="align-middle">
                            </label>
                      </td>

                      <td>
                        <span class="mr-25 w-4 h-4 overflow-hidden text-center border-1 brc-secondary-m2 radius-round shadow-sm d-zoom-4">
                            <img alt="{{$user->name}}'s avatar" src="{{asset('public/backend')}}/assets/image/avatar/avatar3.jpg" class="border-2 brc-white-tp1 radius-round h-6 w-6">
                        </span>
                        {{-- <span class="avatar avatar-md  d-block brround cover-image" data-image-src="{{asset('backend')}}/assets/images/faces/female/25.jpg" style="background: url({{asset('backend')}}/assets/images/faces/female/25.jpg&quot;) center center;"></span> --}}

                        {{-- @if ($user->image !=NULL)
                            <span class="avatar avatar-md  d-block brround cover-image" data-image-src="../assets/images/faces/female/25.jpg" style="background: url(&quot;../assets/images/faces/female/25.jpg&quot;) center center;"></span>
                        @endif

                        @if ($user->icon !=NULL)
                            <i class="{{$user->icon}}" style="font-size:1.25em;"></i>
                        @endif --}}

                      </td>

                      <td class="text-600 text-orange-d2">
                        <span class="text-105">
                            {{$user->name}}
                        </span>
                        <div class="text-95 text-secondary-d1">
                            {{$user->utype}}
                        </div>
                      </td>


                      <td class="d-none d-sm-table-cell text-grey-d1">
                        <span class="text-105">
                            {{$user->first_name}} {{$user->last_name}}
                        </span>
                        <div class="text-95 text-secondary-d1">
                            {{$user->position}}
                        </div>
                      </td>

                      <td class="d-none d-sm-table-cell text-grey-d1">
                        <span class="badge badge-success badge-sm">{{$user->phone}}</span>
                        <br>
                        <span class="badge badge-primary badge-sm">{{$user->email}}</span>
                      </td>



                      <td class="d-none d-sm-table-cell">
                        <span class="badge badge-sm bgc-warning-d1 text-white pb-1 px-25">{{$user->created_at}}</span>
                      </td>



                      <td>
                        <!-- action buttons -->
                        <div class="d-none d-lg-flex">
                          <a data-toggle="modal" data-target="#editModal" wire:click="edit({{ $user->id }})" href="#" class="mx-2px btn radius-1 border-2 btn-xs btn-brc-tp btn-light-secondary btn-h-lighter-success btn-a-lighter-success">
                            <i class="fa fa-pencil-alt"></i>
                          </a>

                          <a wire:click="delete({{ $user->id }})" href="#" class="mx-2px btn radius-1 border-2 btn-xs btn-brc-tp btn-light-danger btn-h-lighter-danger btn-a-lighter-danger">
                            <i class="fa fa-trash-alt"></i>
                          </a>
                          {{-- <a href="#" class="mx-2px btn radius-1 border-2 btn-xs btn-brc-tp btn-light-secondary btn-h-lighter-warning btn-a-lighter-warning">
                            <i class="fa fa-ellipsis-v mx-1"></i>
                          </a> --}}

                        </div>

                        <!-- show a dropdown in mobile -->
                        <div class="dropdown d-inline-block d-lg-none dd-backdrop dd-backdrop-none-lg">
                          <a href="#" class="btn btn-default btn-xs py-15 radius-round dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-cog"></i>
                          </a>

                          <div class="dropdown-menu dd-slide-up dd-slide-none-lg">
                            <div class="dropdown-inner">
                              <div class="dropdown-header text-100 text-secondary-d1 border-b-1 brc-secondary-l2 text-600 mb-2">
                                Action
                              </div>
                              <a data-toggle="modal" data-target="#editModal" wire:click="edit({{ $user->id }})" href="#" class="dropdown-item">
                                <i class="fa fa-pencil-alt text-blue mr-1 p-2 w-4"></i>
                                Edit
                              </a>
                              <a wire:click="delete({{ $user->id }})" href="#" class="dropdown-item">
                                <i class="fa fa-trash-alt text-danger-m1 mr-1 p-2 w-4"></i>
                                Delete
                              </a>

                              {{-- <a href="#" class="dropdown-item">
                                <i class="far fa-flag text-orange-d1 mr-1 p-2 w-4"></i>
                                Flag
                              </a> --}}

                            </div>
                          </div>
                        </div>
                      </td>

                  </tr>

                  @endforeach

                  </tbody>


              </table>

                <!-- table footer -->
                <div class="d-flex pl-4 pr-3 pt-35 border-t-1 brc-secondary-l3 flex-column flex-sm-row mt-n1px">
                  <div class="text-nowrap align-self-center align-self-sm-start">
                    <span class="d-inline-block text-grey-d2">
                      {{-- Showing 1 - 10 of 152 --}}
                      Showing
                  </span>

                    <select wire:model="pagesNo" class="ml-3 ace-select no-border angle-down brc-h-blue-m3 w-auto pr-45 text-secondary-d3">
                      <option value="2">2</option>
                      <option value="10">10</option>
                      <option value="20">20</option>
                      <option value="40">40</option>
                      <option value="60">60</option>
                      <option value="100">100</option>
                    </select>
                  </div>

                  <div class="btn-group ml-sm-auto mt-3 mt-sm-0">
                      {{ $users->links() }}
                  </div>
                </div>



              </form>

            </div><!-- /.card-body -->
          </div><!-- /.card -->
        </div><!-- /.col -->
      </div><!-- /.row -->


