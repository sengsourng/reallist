<div wire:ignore.self class="modal fade" id="openModal" tabindex="-1" role="dialog" aria-labelledby="openModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="openModalLabel">Create Specification</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body pd-5">
                <form>

                        <div class="form-row">
                                <div class="form-group col-md-12">
                                    <div class="input-floating-label text-blue-d2 brc-blue-m1">
                                    <input wire:model="name" type="text" placeholder="Specification Name" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                                        <span class="floating-label text-grey-m3">
                                            Specification Name
                                        </span>
                                    </div>

                                    @error('name') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>


                                <div class="form-group col-md-12">
                                    <div class="input-floating-label text-blue-d2 brc-blue-m1">
                                    <input wire:model="slug" type="text" placeholder="Specification slug" class="form-control form-controlcol-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                                        <span class="floating-label text-grey-m3">
                                            Specification slug
                                        </span>
                                    </div>

                                    @error('slug') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <div class="input-floating-label text-blue-d2 brc-blue-m1">
                                    <input wire:model="icon" type="text" placeholder="Specification icon" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                                        <span class="floating-label text-grey-m3">
                                            Icon(Ex: fa fa-automobile)
                                        </span>
                                    </div>

                                    @error('icon') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>

                                {{-- <div class="form-group col-md-12">

                                    <div class="custom-file">
                                        <input wire:model="image" type="file" class="custom-file-input" name="image">
                                        <label class="custom-file-label">Choose file</label>
                                    </div>
                                    @error('image') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div> --}}


                        </div>




                        <div class="form-group">
                            <label class="col-form-label">Short Description</label>
                            <textarea wire:model="short_description" class="form-control" name="short_description" rows="4" placeholder="Short Description.."></textarea>
                            @error('short_description') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                </form>



            </div><!-- modal-body -->
            <div class="modal-footer">
                <button wire:click.prevent="store()" type="button" class="btn btn-info"><i class="fa fa-save"></i> Save</button>
                <button wire:click="cancel()" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div>
