<div wire:ignore.self class="modal fade modal-fs" id="openModal" tabindex="-1" role="dialog" aria-labelledby="openModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="openModalLabel">
                    <i class="fa fa-edit f-n-hover"></i>
                    Create User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>

                        <div class="form-row">

                            <div class="col-md-6">

                                {{-- User Name & Position --}}
                                <div class="form-group col-md-12">
                                    <div class="form-group row">
                                        <div class="form-group col-md-6">
                                            <div class="input-floating-label text-blue-d2 brc-blue-m1">
                                            <input wire:model="name" type="text" placeholder="Username" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                                                <span class="floating-label text-grey-m3">
                                                    Username
                                                </span>
                                            </div>

                                            @error('name')
                                                <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="input-floating-label text-blue-d2 brc-blue-m1">
                                            <input wire:model="position" type="text" placeholder="Position" class="form-control form-controlcol-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                                                <span class="floating-label text-grey-m3">
                                                    Position
                                                </span>
                                            </div>

                                            @error('position')
                                                <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                                            @enderror
                                        </div>

                                    </div>
                                </div>


                                {{-- First Name and Last Name --}}
                                <div class="form-group col-md-12">
                                    <div class="form-group row">
                                        <div class="form-group col-md-6">
                                            <div class="input-floating-label text-blue-d2 brc-blue-m1">
                                            <input wire:model="first_name" type="text" placeholder="First Name" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                                                <span class="floating-label text-grey-m3">
                                                    First Name
                                                </span>
                                            </div>

                                            @error('first_name')
                                                <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="input-floating-label text-blue-d2 brc-blue-m1">
                                            <input wire:model="last_name" type="text" placeholder="Last Name" class="form-control form-controlcol-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                                                <span class="floating-label text-grey-m3">
                                                    Last Name
                                                </span>
                                            </div>

                                            @error('last_name')
                                                <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                                            @enderror
                                        </div>

                                    </div>
                                </div>

                                 {{-- Email & Password --}}
                                 <div class="form-group col-md-12">
                                    <div class="form-group row">
                                        <div class="form-group col-md-6">
                                            <div class="input-floating-label text-blue-d2 brc-blue-m1">
                                            <input wire:model="email" type="email" placeholder="Email" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                                                <span class="floating-label text-grey-m3">
                                                    Email
                                                </span>
                                            </div>

                                            @error('email')
                                                <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="input-floating-label text-blue-d2 brc-blue-m1">
                                            <input wire:model="password" type="password" placeholder="Password" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                                                <span class="floating-label text-grey-m3">
                                                    Password
                                                </span>
                                            </div>

                                            @error('password')
                                                <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                                            @enderror
                                        </div>

                                    </div>
                                </div>


                                 {{-- Phone & User Type --}}
                                 <div class="form-group col-md-12">
                                    <div class="form-group row">
                                        <div class="form-group col-md-6">
                                            <div class="input-floating-label text-blue-d2 brc-blue-m1">
                                            <input wire:model="phone" type="text" placeholder="Phone" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                                                <span class="floating-label text-grey-m3">
                                                    Phone
                                                </span>
                                            </div>

                                            @error('phone')
                                                <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group col-md-6">
                                            <select wire:model="utype" name="utype" class="form-control" id="form-field-select-1">
                                                <option value="">Select User Type</option>
                                                <option value="ADM">Admin</option>
                                                <option value="USR">User</option>
                                                <option value="agency">Agency</option>
                                                <option value="customer">Customer</option>

                                              </select>

                                            @error('utype')
                                                <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                                            @enderror
                                        </div>

                                    </div>
                                </div>




                                <div class="form-group col-md-12">
                                    <label class="col-form-label">About Me</label>
                                    <textarea wire:model="about_me" class="form-control" name="about_me" rows="4" placeholder="Short Description.."></textarea>
                                    @error('about_me') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>


                            </div>

                            <div class="col-md-6">
                                <div class="card" id="card-1" draggable="false" style="">
                                    <div class="card-header">
                                      <h5 class="card-title">
                                       Address Information
                                      </h5>
                                    </div><!-- /.card-header -->

                                    <div class="card-body p-0 collapse show" style="">
                                      <!-- to have smooth .card toggling, it should have zero padding -->
                                      <div class="p-3">
                                        <div class="form-group col-md-12">
                                            <div class="input-floating-label text-blue-d2 brc-blue-m1">
                                            <input wire:model="address" type="text" placeholder="Address" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                                                <span class="floating-label text-grey-m3">
                                                    Address
                                                </span>
                                            </div>

                                            @error('address') <span class="text-red-500">{{ $message }}</span>@enderror
                                        </div>


                                        <div class="form-group col-md-12">
                                            <div class="input-floating-label text-blue-d2 brc-blue-m1">
                                            <input wire:model="city" type="text" placeholder="City" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                                                <span class="floating-label text-grey-m3">
                                                    City
                                                </span>
                                            </div>

                                            @error('city') <span class="text-red-500">{{ $message }}</span>@enderror
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="input-floating-label text-blue-d2 brc-blue-m1">
                                            <input wire:model="postal_code" type="text" placeholder="Postal Code" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                                                <span class="floating-label text-grey-m3">
                                                    Postal Code
                                                </span>
                                            </div>

                                            @error('postal_code') <span class="text-red-500">{{ $message }}</span>@enderror
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="input-floating-label text-blue-d2 brc-blue-m1">
                                            <input wire:model="country" type="text" placeholder="Country" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                                                <span class="floating-label text-grey-m3">
                                                    Country
                                                </span>
                                            </div>

                                            @error('country') <span class="text-red-500">{{ $message }}</span>@enderror
                                        </div>

                                      </div>
                                    </div><!-- /.card-body -->
                                  </div>


                            </div>









                                {{-- <div class="form-group col-md-12">

                                    <div class="custom-file">
                                        <input wire:model="image" type="file" class="custom-file-input" name="image">
                                        <label class="custom-file-label">Choose file</label>
                                    </div>
                                    @error('image') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div> --}}


                        </div>


                </form>



            </div><!-- modal-body -->
            <div class="modal-footer">
                <button wire:click.prevent="store()" type="button" class="btn btn-info"><i class="fa fa-save"></i> Save</button>
                <button wire:click="cancel()" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div>
