		{{-- Sliders Section --}}
		<div>
			<div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg">
				<div class="header-text1 mb-0">
					<div class="container">
						<div class="row">
							<div class="col-xl-8 col-lg-12 col-md-12 d-block mx-auto">
								<div class="text-center text-white text-property">
									<h1 class=""><span class="font-weight-bold">16,25,365&nbsp;</span>Properties Available</h1>
								</div>
								<div class="search-background bg-transparent mb-0">
									<div class="form row no-gutters">
										<div class="form-group  col-xl-6 col-lg-5 col-md-12 mb-0 bg-white">
											<input type="text" class="form-control input-lg border-right-0" id="text" placeholder="Search Property">
										</div>
										<div class="form-group col-xl-4 col-lg-4 select2-lg  col-md-12 mb-0 bg-white">
											<select class="form-control select2-show-search border-bottom-0 w-100" data-placeholder="Select">
												<optgroup label="Categories">
													<option>Select</option>
													<option value="1">Homes</option>
													<option value="2">Apartments</option>
													<option value="3">Offices</option>
													<option value="4">Apartments</option>
													<option value="5">Duplex Houses</option>
													<option value="6">Luxury Rooms</option>
													<option value="7">Flats</option>
													<option value="8">Modren Homes </option>
													<option value="9">Deluxe Houses</option>
													<option value="10">3BHK Flats</option>
												</optgroup>
											</select>
										</div>
										<div class="col-xl-2 col-lg-3 col-md-12 mb-0">
											<a href="#" class="btn btn-lg btn-block btn-primary br-bl-0 br-tl-0">Search</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                {{-- /header-text --}}
			</div>
		</div>
		{{-- /Sliders Section --}}

		{{-- Breadcrumb --}}
		<div class="bg-white border-bottom">
			<div class="container">
				<div class="page-header">
					<h4 class="page-title">RealEstate list</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item"><a href="#">Pages</a></li>
						<li class="breadcrumb-item active" aria-current="page">RealEstate</li>
					</ol>
				</div>
			</div>
		</div>
		{{-- Breadcrumb --}}

		{{-- Add listing --}}
		<section class="sptb">
			<div class="container">
				<div class="row">
					<div class="col-xl-9 col-lg-8 col-md-12">
						{{-- Add lists --}}
						<div class=" mb-lg-0">
							<div class="">
								<div class="item2-gl ">
									<div class=" mb-0">
										<div class="">
											<div class="p-5 bg-white item2-gl-nav d-flex">
												<h6 class="mb-0 mt-2">Showing 1 to 10 of 30 entries</h6>
												<ul class="nav item2-gl-menu ml-auto">
													<li class=""><a href="#tab-11" class="active show" data-toggle="tab" title="List style"><i class="fa fa-list"></i></a></li>
													<li><a href="#tab-12" data-toggle="tab" class="" title="Grid"><i class="fa fa-th"></i></a></li>
												</ul>
												<div class="d-flex">
													<label class="mr-2 mt-1 mb-sm-1">Sort By:</label>
													<select name="item" class="form-control select-sm w-75 select2">
														<option value="1">Latest</option>
														<option value="2">Oldest</option>
														<option value="3">Price:Low-to-High</option>
														<option value="5">Price:Hight-to-Low</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-content">
										<div class="tab-pane active" id="tab-11">
											<div class="card overflow-hidden">
												<div class="d-md-flex">
													<div class="item-card9-img">
														<div class="arrow-ribbon bg-primary">$263.99</div>
														<div class="item-card9-imgs">
															<a href="col-left.html"></a>
															<img src="../assets/images/products/h4.png" alt="img" class="cover-image">
														</div>
														<div class="item-card9-icons">
															<a href="#" class="item-card9-icons1 wishlist" data-toggle="tooltip" data-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
															<a href="#" class="item-card9-icons1 bg-purple" data-toggle="tooltip" data-placement="top" title="Share"> <i class="icon icon-share" ></i></a>
														</div>
														<div class="item-tags">
															<div class="bg-success tag-option">For Sale </div>
															<div class="bg-pink tag-option">Open </div>
														</div>
														<div class="item-trans-rating">
															<div class="rating-stars block">
																<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
																<div class="rating-stars-container">
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm ">
																		<i class="fa fa-star"></i>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="card border-0 mb-0">
														<div class="card-body ">
															<div class="item-card9">
																<a href="col-left.html" class="text-muted mr-4"><i class="fa fa-tag mr-1"></i> Villa</a>
																<a href="col-left.html" class="text-dark"><h4 class="font-weight-bold mt-1">2BHK flat </h4></a>
																<div class="mb-2">
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-arrows-alt text-muted mr-1"></i> 950 Sqft</a>
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-bed text-muted mr-1"></i> 4 Beds</a>
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a>
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-car text-muted mr-1"></i> 2 Car</a>
																</div>
																<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit</p>
															</div>
														</div>
														<div class="card-footer pt-4 pb-4">
															<div class="item-card9-footer d-flex">
																<div class="item-card9-cost">
																	<a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a>
																	<a href="#" class=""><span class=""><i class="fa fa-user text-muted mr-1"></i> Owner</span></a>
																</div>
																<div class="ml-auto">
																	<a href="#" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 2 days ago</span></a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="card overflow-hidden">
												<div class="d-md-flex">
													<div class="item-card9-img">
														<div class="arrow-ribbon bg-secondary">$987.88</div>
														<div class="item-card9-imgs">
															<a href="col-left.html"></a>
															<div id="carousel-controls1" class="carousel slide property-slide" data-ride="carousel" data-interval="false">
																<div class="carousel-inner">
																	<div class="carousel-item active">
																		<img src="../assets/images/products/j2.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																	</div>
																	<div class="carousel-item">
																		<img src="../assets/images/products/f2.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																	</div>
																	<div class="carousel-item">
																		<img src="../assets/images/products/f4.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																	</div>
																	<div class="carousel-item">
																		<img src="../assets/images/products/b3.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																	</div>
																	<div class="carousel-item">
																		<img src="../assets/images/products/e1.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																	</div>
																</div>
																<a class="carousel-control-prev" href="#carousel-controls1" role="button" data-slide="prev">
																	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																	<span class="sr-only">Previous</span>
																</a>
																<a class="carousel-control-next" href="#carousel-controls1" role="button" data-slide="next">
																	<span class="carousel-control-next-icon" aria-hidden="true"></span>
																	<span class="sr-only">Next</span>
																</a>
															</div>
														</div>
														<div class="item-card9-icons">
															<a href="#" class="item-card9-icons1 wishlist active" data-toggle="tooltip" data-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
															<a href="#" class="item-card9-icons1 bg-purple" data-toggle="tooltip" data-placement="top" title="Share"> <i class="icon icon-share" ></i></a>
														</div>
														<div class="item-tags">
															<div class="bg-success tag-option">For Sale </div>
														</div>
														<div class="item-trans-rating">
															<div class="rating-stars block">
																<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="2">
																<div class="rating-stars-container">
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm ">
																		<i class="fa fa-star"></i>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="card border-0 mb-0">
														<div class="card-body ">
															<div class="item-card9">
																<a href="col-left.html" class="text-muted"><i class="fa fa-tag mr-1"></i> Deluxe House</a>
																<a href="col-left.html" class="text-dark"><h4 class="font-weight-bold mt-1">Luxury Home For Sale</h4></a>
																<div class="mb-2">
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-arrows-alt text-muted mr-1"></i> 950 Sqft</a>
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-bed text-muted mr-1"></i> 4 Beds</a>
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a>
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-car text-muted mr-1"></i> 2 Car</a>
																</div>
																<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit</p>
															</div>
														</div>
														<div class="card-footer pt-4 pb-4">
															<div class="item-card9-footer d-flex">
																<div class="item-card9-cost">
																	<a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a>
																	<a href="#" class=""><span class=""><i class="fa fa-user text-muted mr-1"></i> Agent</span></a>
																</div>
																<div class="ml-auto">
																	<a href="#" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 15 mins ago</span></a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="sold-out1">
												<div class="ribbon sold-ribbon ribbon-top-left text-danger"><span class="bg-danger">Sold Out</span></div>
												<div class="card overflow-hidden">
													<div class="d-md-flex">
														<div class="item-card9-img">
															<div class="arrow-ribbon bg-success">$567</div>
															<div class="item-card9-imgs">
																<a href="col-left.html"></a>
																<img src="../assets/images/products/pe1.png" alt="img" class="cover-image">
															</div>
															<div class="item-card9-icons">
																<a href="#" class="item-card9-icons1 wishlist" data-toggle="tooltip" data-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
																<a href="#" class="item-card9-icons1 bg-purple" data-toggle="tooltip" data-placement="top" title="Share"> <i class="icon icon-share" ></i></a>
															</div>
															<div class="item-tags">
																<div class="bg-danger tag-option">For Buy </div>
															</div>
															<div class="item-trans-rating">
																<div class="rating-stars block">
																	<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4">
																	<div class="rating-stars-container">
																		<div class="rating-star sm  ">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm  ">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm  ">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm ">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm ">
																			<i class="fa fa-star"></i>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="card border-0 mb-0">
															<div class="card-body ">
																<div class="item-card9">
																	<a href="col-left.html" class="text-muted"><i class="fa fa-tag mr-1"></i> 3BHK Flats</a>
																	<a href="col-left.html" class="text-dark"><h4 class="font-weight-bold mt-1">Apartment For Rent </h4></a>
																	<div class="mb-2">
																		<a href="#" class="icons text-muted mr-4"><i class="fa fa-arrows-alt text-muted mr-1"></i> 400 Sqft</a>
																		<a href="#" class="icons text-muted mr-4"><i class="fa fa-bed text-muted mr-1"></i> 3 Beds</a>
																		<a href="#" class="icons text-muted mr-4"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a>
																		<a href="#" class="icons text-muted mr-4"><i class="fa fa-car text-muted mr-1"></i> 1 Car</a>
																	</div>
																	<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit</p>
																</div>
															</div>
															<div class="card-footer pt-4 pb-4">
																<div class="item-card9-footer d-flex">
																	<div class="item-card9-cost">
																		<a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a>
																		<a href="#" class=""><span class=""><i class="fa fa-user text-muted mr-1"></i> Owner</span></a>
																	</div>
																	<div class="ml-auto">
																		<a href="#" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 1 days ago</span></a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="card overflow-hidden">
												<div class="d-md-flex">
													<div class="item-card9-img">
														<div class="arrow-ribbon bg-pink">$567</div>
														<div class="item-card9-imgs">
															<a href="col-left.html"></a>
															<img src="../assets/images/products/b3.png" alt="img" class="cover-image">
														</div>
														<div class="item-card9-icons">
															<a href="#" class="item-card9-icons1 wishlist active" data-toggle="tooltip" data-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
															<a href="#" class="item-card9-icons1 bg-purple" data-toggle="tooltip" data-placement="top" title="Share"> <i class="icon icon-share" ></i></a>
														</div>
														<div class="item-tags">
															<div class="bg-success tag-option">For Sale </div>
															<div class="bg-pink tag-option">Hot </div>
														</div>
														<div class="item-trans-rating">
															<div class="rating-stars block">
																<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
																<div class="rating-stars-container">
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm ">
																		<i class="fa fa-star"></i>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="card border-0 mb-0">
														<div class="card-body ">
															<div class="item-card9">
																<a href="col-left.html" class="text-muted"><i class="fa fa-tag mr-1"></i> Office</a>
																<a href="col-left.html" class="text-dark"><h4 class="font-weight-bold mt-1"> Office rooms.... </h4></a>
																<div class="mb-2">
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-arrows-alt text-muted mr-1"></i> 1500 Sqft</a>
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-bed text-muted mr-1"></i> 5 Beds</a>
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a>
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-car text-muted mr-1"></i> 2 Car</a>
																</div>
																<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit</p>
															</div>
														</div>
														<div class="card-footer pt-4 pb-4">
															<div class="item-card9-footer d-flex">
																<div class="item-card9-cost">
																	<a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a>
																	<a href="#" class=""><span class=""><i class="fa fa-user text-muted mr-1"></i> Agent</span></a>
																</div>
																<div class="ml-auto">
																	<a href="#" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 35 mins ago</span></a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="card overflow-hidden">
												<div class="d-md-flex">
													<div class="item-card9-img">
														<div class="arrow-ribbon bg-primary">$839</div>
														<div class="item-card9-imgs">
															<a href="col-left.html"></a>
															<img src="../assets/images/products/f4.png" alt="img" class="cover-image">
														</div>
														<div class="item-card9-icons">
															<a href="#" class="item-card9-icons1 wishlist" data-toggle="tooltip" data-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
															<a href="#" class="item-card9-icons1 bg-purple" data-toggle="tooltip" data-placement="top" title="Share"> <i class="icon icon-share" ></i></a>
														</div>
														<div class="item-tags">
															<div class="bg-info tag-option">For Rent </div>
														</div>
														<div class="item-trans-rating">
															<div class="rating-stars block">
																<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="1">
																<div class="rating-stars-container">
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm ">
																		<i class="fa fa-star"></i>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="card border-0 mb-0">
														<div class="card-body ">
															<div class="item-card9">
																<a href="col-left.html" class="text-muted"><i class="fa fa-tag mr-1"></i> Luxury rooms</a>
																<a href="col-left.html" class="text-dark"><h4 class="font-weight-bold mt-1">Apartment For Rent</h4></a>
																<div class="mb-2">
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-arrows-alt text-muted mr-1"></i> 300 Sqft</a>
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-bed text-muted mr-1"></i> 2 Beds</a>
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-bath text-muted mr-1"></i> 2 Bath</a>
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-car text-muted mr-1"></i> 1 Car</a>
																</div>
																<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit</p>
															</div>
														</div>
														<div class="card-footer pt-4 pb-4">
															<div class="item-card9-footer d-flex">
																<div class="item-card9-cost">
																	<a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a>
																	<a href="#" class=""><span class=""><i class="fa fa-user text-muted mr-1"></i> Agent</span></a>
																</div>
																<div class="ml-auto">
																	<a href="#" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 5 days ago</span></a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="card overflow-hidden">
												<div class="d-md-flex">
													<div class="item-card9-img">
														<div class="arrow-ribbon bg-secondary">$289</div>
														<div class="item-card9-imgs">
															<a href="col-left.html"></a>
															<img src="../assets/images/products/v1.png" alt="img" class="cover-image">
														</div>
														<div class="item-card9-icons">
															<a href="#" class="item-card9-icons1 wishlist active" data-toggle="tooltip" data-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
															<a href="#" class="item-card9-icons1 bg-purple" data-toggle="tooltip" data-placement="top" title="Share"> <i class="icon icon-share" ></i></a>
														</div>
														<div class="item-tags">
															<div class="bg-success tag-option">For Sale </div>
															<div class="bg-pink tag-option">New</div>
														</div>
														<div class="item-trans-rating">
															<div class="rating-stars block">
																<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="1">
																<div class="rating-stars-container">
																	<div class="rating-star sm ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm  ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm ">
																		<i class="fa fa-star"></i>
																	</div>
																	<div class="rating-star sm ">
																		<i class="fa fa-star"></i>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="card border-0 mb-0">
														<div class="card-body ">
															<div class="item-card9">
																<a href="col-left.html" class="text-muted"><i class="fa fa-tag mr-1"></i> Apartments</a>
																<a href="col-left.html" class="text-dark"><h4 class="font-weight-bold mt-1">Apartment For Rent </h4></a>
																<div class="mb-2">
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-arrows-alt text-muted mr-1"></i> 2500 Sqft</a>
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-bed text-muted mr-1"></i> 20 Beds</a>
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-bath text-muted mr-1"></i> 15 Bath</a>
																	<a href="#" class="icons text-muted mr-4"><i class="fa fa-car text-muted mr-1"></i> 10 Car</a>
																</div>
																<p class="mb-0 leading-tight">Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit</p>
															</div>
														</div>
														<div class="card-footer pt-4 pb-4">
															<div class="item-card9-footer d-flex">
																<div class="item-card9-cost">
																	<a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a>
																	<a href="#" class=""><span class=""><i class="fa fa-user text-muted mr-1"></i> Owner</span></a>
																</div>
																<div class="ml-auto">
																	<a href="#" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 3 days ago</span></a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane" id="tab-12">
											<div class="row">
												<div class="col-lg-6 col-md-12 col-xl-4">
													<div class="card overflow-hidden">
														<div class="item-card9-img">
															<div class="arrow-ribbon bg-primary">$263.99</div>
															<div class="item-card9-imgs">
																<a href="col-left.html"></a>
																<img src="../assets/images/products/h4.png" alt="img" class="cover-image">
															</div>
															<div class="item-card9-icons">
																<a href="#" class="item-card9-icons1 wishlist" data-toggle="tooltip" data-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
																<a href="#" class="item-card9-icons1 bg-purple" data-toggle="tooltip" data-placement="top" title="Share"> <i class="icon icon-share" ></i></a>
															</div>
															<div class="item-tags">
																<div class="bg-success tag-option">For Sale </div>
																<div class="bg-pink tag-option">Open </div>
															</div>
															<div class="item-trans-rating">
																<div class="rating-stars block">
																	<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value"  value="3">
																	<div class="rating-stars-container">
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="card-body">
															<div class="item-card9">
																<a href="col-left.html" class="text-muted mr-4"><i class="fa fa-tag mr-1"></i> Villa</a>
																<a href="#" class=""><span class="text-muted"><i class="fa fa-user text-muted mr-1"></i> Owner</span></a>
																<a href="col-left.html" class="text-dark mt-2"><h4 class="font-weight-bold mt-1">2BHK flat </h4></a>
																<ul class="item-card2-list">
																	<li><a href="#" class="icons text-muted"><i class="fa fa-arrows-alt text-muted mr-1"></i> 700 Sqft</a></li>
																	<li><a href="#" class="icons text-muted"><i class="fa fa-bed text-muted mr-1"></i> 5 Beds</a></li>
																	<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-bath text-muted mr-1"></i> 4 Bath</a></li>
																	<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-car text-muted mr-1"></i> 2 Car</a></li>
																</ul>
																<p class="mb-0">Ut enim ad minima veniamq nostrum exerci </p>
															</div>
														</div>
														<div class="card-footer">
															<div class="item-card9-footer d-flex">
																<div class="item-card9-cost">
																	<a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a>
																</div>
																<div class="ml-auto">
																	<a href="#" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 2 days ago</span></a>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-6 col-md-12 col-xl-4">
													<div class="card overflow-hidden">
														<div class="arrow-ribbon bg-secondary">$987.88</div>
														<div class="item-card9-img">
															<div class="item-card9-imgs">
																<a href="col-left.html"></a>
																<div id="carousel-controls" class="carousel slide property-slide" data-ride="carousel" data-interval="false">
																	<div class="carousel-inner">
																		<div class="carousel-item active">
																			<img src="../assets/images/products/j2.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																		</div>
																		<div class="carousel-item">
																			<img src="../assets/images/products/f2.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																		</div>
																		<div class="carousel-item">
																			<img src="../assets/images/products/f4.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																		</div>
																		<div class="carousel-item">
																			<img src="../assets/images/products/b3.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																		</div>
																		<div class="carousel-item">
																			<img src="../assets/images/products/e1.png" alt="img" class="cover-image d-block w-100" data-holder-rendered="true">
																		</div>
																	</div>
																	<a class="carousel-control-prev" href="#carousel-controls" role="button" data-slide="prev">
																		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																		<span class="sr-only">Previous</span>
																	</a>
																	<a class="carousel-control-next" href="#carousel-controls" role="button" data-slide="next">
																		<span class="carousel-control-next-icon" aria-hidden="true"></span>
																		<span class="sr-only">Next</span>
																	</a>
																</div>
															</div>
															<div class="item-card9-icons">
																<a href="#" class="item-card9-icons1 wishlist active" data-toggle="tooltip" data-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
																<a href="#" class="item-card9-icons1 bg-purple" data-toggle="tooltip" data-placement="top" title="Share"> <i class="icon icon-share" ></i></a>
															</div>
															<div class="item-tags">
																<div class="bg-success tag-option">For Sale </div>
															</div>
															<div class="item-trans-rating">
																<div class="rating-stars block">
																	<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value"  value="2">
																	<div class="rating-stars-container">
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="card-body">
															<div class="item-card9">
																<a href="col-left.html" class="text-muted mr-4"><i class="fa fa-tag mr-1"></i> Deluxe House</a>
																<a href="#" class=""><span class="text-muted"><i class="fa fa-user text-muted mr-1"></i> Agent</span></a>
																<a href="col-left.html" class="text-dark mt-2"><h4 class="font-weight-bold mt-1">Luxury Home For Sale</h4></a>
																<ul class="item-card2-list">
																	<li><a href="#" class="icons text-muted"><i class="fa fa-arrows-alt text-muted mr-1"></i> 950 Sqft</a></li>
																	<li><a href="#" class="icons text-muted"><i class="fa fa-bed text-muted mr-1"></i> 4 Beds</a></li>
																	<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a></li>
																	<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-car text-muted mr-1"></i> 2 Car</a></li>
																</ul>
																<p class="mb-0">Ut enim ad minima veniamq nostrum exerci ullam</p>
															</div>
														</div>
														<div class="card-footer">
															<div class="item-card9-footer d-flex">
																<div class="item-card9-cost">
																	<a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a>
																</div>
																<div class="ml-auto">
																	<a href="#" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 15 mins ago</span></a>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-6 col-md-12 col-xl-4">
													<div class="sold-out1">
														<div class="ribbon sold-ribbon ribbon-top-left text-danger"><span class="bg-danger">Sold Out</span></div>
														<div class="card overflow-hidden">
															<div class="arrow-ribbon bg-success">$567</div>
															<div class="item-card9-img">
																<div class="item-card9-imgs">
																	<a href="col-left.html"></a>
																	<img src="../assets/images/products/pe1.png" alt="img" class="cover-image">
																</div>
																<div class="item-card9-icons">
																	<a href="#" class="item-card9-icons1 wishlist" data-toggle="tooltip" data-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
																	<a href="#" class="item-card9-icons1 bg-purple" data-toggle="tooltip" data-placement="top" title="Share"> <i class="icon icon-share" ></i></a>
																</div>
																<div class="item-tags">
																	<div class="bg-danger tag-option">For Buy </div>
																</div>
																<div class="item-trans-rating">
																	<div class="rating-stars block">
																		<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value"  value="4">
																		<div class="rating-stars-container">
																			<div class="rating-star sm">
																				<i class="fa fa-star"></i>
																			</div>
																			<div class="rating-star sm">
																				<i class="fa fa-star"></i>
																			</div>
																			<div class="rating-star sm">
																				<i class="fa fa-star"></i>
																			</div>
																			<div class="rating-star sm">
																				<i class="fa fa-star"></i>
																			</div>
																			<div class="rating-star sm">
																				<i class="fa fa-star"></i>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="card-body">
																<div class="item-card9">
																	<a href="col-left.html" class="text-muted mr-4"><i class="fa fa-tag mr-1"></i> 3BHK Flats</a>
																	<a href="#" class=""><span class="text-muted"><i class="fa fa-user text-muted mr-1"></i> Owner</span></a>
																	<a href="col-left.html" class="text-dark mt-2"><h4 class="font-weight-bold mt-1">Apartment For Rent</h4></a>
																	<ul class="item-card2-list">
																		<li><a href="#" class="icons text-muted"><i class="fa fa-arrows-alt text-muted mr-1"></i> 400 Sqft</a></li>
																		<li><a href="#" class="icons text-muted"><i class="fa fa-bed text-muted mr-1"></i> 3 Beds</a></li>
																		<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a></li>
																		<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-car text-muted mr-1"></i> 1 Car</a></li>
																	</ul>
																	<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
																</div>
															</div>
															<div class="card-footer">
																<div class="item-card9-footer d-flex">
																	<div class="item-card9-cost">
																		<a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a>
																	</div>
																	<div class="ml-auto">
																		<a href="#" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 1 days ago</span></a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-6 col-md-12 col-xl-4">
													<div class="card overflow-hidden">
														<div class="arrow-ribbon bg-pink">$567</div>
														<div class="item-card9-img">
															<div class="item-card9-imgs">
																<a href="col-left.html"></a>
																<img src="../assets/images/products/b3.png" alt="img" class="cover-image">
															</div>
															<div class="item-card9-icons">
																<a href="#" class="item-card9-icons1 wishlist active" data-toggle="tooltip" data-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
																<a href="#" class="item-card9-icons1 bg-purple" data-toggle="tooltip" data-placement="top" title="Share"> <i class="icon icon-share" ></i></a>
															</div>
															<div class="item-tags">
																<div class="bg-success tag-option">For Sale</div>
															</div>
															<div class="item-trans-rating">
																<div class="rating-stars block">
																	<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value"  value="3">
																	<div class="rating-stars-container">
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="card-body">
															<div class="item-card9">
																<a href="col-left.html" class="text-muted mr-4"><i class="fa fa-tag mr-1"></i> Office</a>
																<a href="#" class=""><span class="text-muted"><i class="fa fa-user text-muted mr-1"></i> Agent</span></a>
																<a href="col-left.html" class="text-dark mt-2"><h4 class="font-weight-bold mt-1">Office rooms..</h4></a>
																<ul class="item-card2-list">
																	<li><a href="#" class="icons text-muted"><i class="fa fa-arrows-alt text-muted mr-1"></i> 1500 Sqft</a></li>
																	<li><a href="#" class="icons text-muted"><i class="fa fa-bed text-muted mr-1"></i> 5 Beds</a></li>
																	<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a></li>
																	<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-car text-muted mr-1"></i> 2 Car</a></li>
																</ul>
																<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
															</div>
														</div>
														<div class="card-footer">
															<div class="item-card9-footer d-flex">
																<div class="item-card9-cost">
																	<a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a>
																</div>
																<div class="ml-auto">
																	<a href="#" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 35 mins ago</span></a>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-6 col-md-12 col-xl-4">
													<div class="card overflow-hidden">
														<div class="arrow-ribbon bg-primary">$839</div>
														<div class="item-card9-img">
															<div class="item-card9-imgs">
																<a href="col-left.html"></a>
																<img src="../assets/images/products/f4.png" alt="img" class="cover-image">
															</div>
															<div class="item-card9-icons">
																<a href="#" class="item-card9-icons1 wishlist" data-toggle="tooltip" data-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
																<a href="#" class="item-card9-icons1 bg-purple" data-toggle="tooltip" data-placement="top" title="Share"> <i class="icon icon-share" ></i></a>
															</div>
															<div class="item-tags">
																<div class="bg-info tag-option">For Rent </div>
																<div class="bg-pink tag-option">Hot </div>
															</div>
															<div class="item-trans-rating">
																<div class="rating-stars block">
																	<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value"  value="1">
																	<div class="rating-stars-container">
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="card-body">
															<div class="item-card9">
																<a href="col-left.html" class="text-muted mr-4"><i class="fa fa-tag mr-1"></i> Luxury rooms</a>
																<a href="#" class=""><span class="text-muted"><i class="fa fa-user text-muted mr-1"></i> Agent</span></a>
																<a href="col-left.html" class="text-dark mt-2"><h4 class="font-weight-bold mt-1">Apartment For Rent</h4></a>
																<ul class="item-card2-list">
																	<li><a href="#" class="icons text-muted"><i class="fa fa-arrows-alt text-muted mr-1"></i> 300 Sqft</a></li>
																	<li><a href="#" class="icons text-muted"><i class="fa fa-bed text-muted mr-1"></i> 2 Beds</a></li>
																	<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-bath text-muted mr-1"></i> 2 Bath</a></li>
																	<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-car text-muted mr-1"></i> 1 Car</a></li>
																</ul>
																<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
															</div>
														</div>
														<div class="card-footer">
															<div class="item-card9-footer d-flex">
																<div class="item-card9-cost">
																	<a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a>
																</div>
																<div class="ml-auto">
																	<a href="#" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 5 days ago</span></a>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-6 col-md-12 col-xl-4">
													<div class="card overflow-hidden">
														<div class="arrow-ribbon bg-secondary">$289</div>
														<div class="item-card9-img">
															<div class="item-card9-imgs">
																<a href="col-left.html"></a>
																<img src="../assets/images/products/v1.png" alt="img" class="cover-image">
															</div>
															<div class="item-card9-icons">
																<a href="#" class="item-card9-icons1 wishlist active" data-toggle="tooltip" data-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
																<a href="#" class="item-card9-icons1 bg-purple" data-toggle="tooltip" data-placement="top" title="Share"> <i class="icon icon-share" ></i></a>
															</div>
															<div class="item-tags">
																<div class="bg-success tag-option">For Sale </div>
															</div>
															<div class="item-trans-rating">
																<div class="rating-stars block">
																	<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value"  value="1">
																	<div class="rating-stars-container">
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="card-body">
															<div class="item-card9">
																<a href="col-left.html" class="text-muted mr-4"><i class="fa fa-tag mr-1"></i> Apartments</a>
																<a href="#" class=""><span class="text-muted"><i class="fa fa-user text-muted mr-1"></i> Owner</span></a>
																<a href="col-left.html" class="text-dark mt-2"><h4 class="font-weight-bold mt-1">Apartment For Rent</h4></a>
																<ul class="item-card2-list">
																	<li><a href="#" class="icons text-muted"><i class="fa fa-arrows-alt text-muted mr-1"></i> 2500 Sqft</a></li>
																	<li><a href="#" class="icons text-muted"><i class="fa fa-bed text-muted mr-1"></i> 20 Beds</a></li>
																	<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-bath text-muted mr-1"></i> 15 Bath</a></li>
																	<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-car text-muted mr-1"></i> 10 Car</a></li>
																</ul>
																<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
															</div>
														</div>
														<div class="card-footer">
															<div class="item-card9-footer d-flex">
																<div class="item-card9-cost">
																	<a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a>
																</div>
																<div class="ml-auto">
																	<a href="#" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 3 days ago</span></a>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-6 col-md-12 col-xl-4">
													<div class="card overflow-hidden">
														<div class="arrow-ribbon bg-primary">$289</div>
														<div class="item-card9-img">
															<div class="item-card9-imgs">
																<a href="col-left.html"></a>
																<img src="../assets/images/products/j2.png" alt="img" class="cover-image">
															</div>
															<div class="item-card9-icons">
																<a href="#" class="item-card9-icons1 wishlist active" data-toggle="tooltip" data-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
																<a href="#" class="item-card9-icons1 bg-purple" data-toggle="tooltip" data-placement="top" title="Share"> <i class="icon icon-share" ></i></a>
															</div>
															<div class="item-tags">
																<div class="bg-info tag-option">For Rent </div>
															</div>
															<div class="item-trans-rating">
																<div class="rating-stars block">
																	<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value"  value="2">
																	<div class="rating-stars-container">
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="card-body">
															<div class="item-card9">
																<a href="col-left.html" class="text-muted mr-4"><i class="fa fa-tag mr-1"></i> 2BHK House</a>
																<a href="#" class=""><span class="text-muted"><i class="fa fa-user text-muted mr-1"></i> Owner</span></a>
																<a href="col-left.html" class="text-dark mt-2"><h4 class="font-weight-bold mt-1">2BHK house For Rent</h4></a>
																<ul class="item-card2-list">
																	<li><a href="#" class="icons text-muted"><i class="fa fa-arrows-alt text-muted mr-1"></i> 160 Sqft</a></li>
																	<li><a href="#" class="icons text-muted"><i class="fa fa-bed text-muted mr-1"></i> 2 Beds</a></li>
																	<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a></li>
																	<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-car text-muted mr-1"></i> 1 Car</a></li>
																</ul>
																<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
															</div>
														</div>
														<div class="card-footer">
															<div class="item-card9-footer d-flex">
																<div class="item-card9-cost">
																	<a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a>
																</div>
																<div class="ml-auto">
																	<a href="#" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 2 days ago</span></a>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-6 col-md-12 col-xl-4">
													<div class="card overflow-hidden">
														<div class="arrow-ribbon bg-secondary">$729</div>
														<div class="item-card9-img">
															<div class="item-card9-imgs">
																<a href="col-left.html"></a>
																<img src="../assets/images/products/h3.png" alt="img" class="cover-image">
															</div>
															<div class="item-card9-icons">
																<a href="#" class="item-card9-icons1 wishlist active" data-toggle="tooltip" data-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
																<a href="#" class="item-card9-icons1 bg-purple" data-toggle="tooltip" data-placement="top" title="Share"> <i class="icon icon-share" ></i></a>
															</div>
															<div class="item-tags">
																<div class="bg-success tag-option">For Sale </div>
																<div class="bg-pink tag-option">New </div>
															</div>
															<div class="item-trans-rating">
																<div class="rating-stars block">
																	<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value"  value="3">
																	<div class="rating-stars-container">
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="card-body">
															<div class="item-card9">
																<a href="col-left.html" class="text-muted mr-4"><i class="fa fa-tag mr-1"></i> Duplex House</a>
																<a href="#" class=""><span class="text-muted"><i class="fa fa-user text-muted mr-1"></i> Owner</span></a>
																<a href="col-left.html" class="text-dark mt-2"><h4 class="font-weight-bold mt-1">Duplex House For Sale</h4></a>
																<ul class="item-card2-list">
																	<li><a href="#" class="icons text-muted"><i class="fa fa-arrows-alt text-muted mr-1"></i> 1786 Sqft</a></li>
																	<li><a href="#" class="icons text-muted"><i class="fa fa-bed text-muted mr-1"></i> 4 Beds</a></li>
																	<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-bath text-muted mr-1"></i> 5 Bath</a></li>
																	<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-car text-muted mr-1"></i> 2 Car</a></li>
																</ul>
																<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
															</div>
														</div>
														<div class="card-footer">
															<div class="item-card9-footer d-flex">
																<div class="item-card9-cost">
																	<a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a>
																</div>
																<div class="ml-auto">
																	<a href="#" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 4 days ago</span></a>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-6 col-md-12 col-xl-4">
													<div class="card overflow-hidden">
														<div class="arrow-ribbon bg-secondary">$389</div>
														<div class="item-card9-img">
															<div class="item-card9-imgs">
																<a href="col-left.html"></a>
																<img src="../assets/images/products/j1.png" alt="img" class="cover-image">
															</div>
															<div class="item-card9-icons">
																<a href="#" class="item-card9-icons1 wishlist active" data-toggle="tooltip" data-placement="top" title="wishlist"> <i class="fa fa fa-heart-o"></i></a>
																<a href="#" class="item-card9-icons1 bg-purple" data-toggle="tooltip" data-placement="top" title="Share"> <i class="icon icon-share" ></i></a>
															</div>
															<div class="item-tags">
																<div class="bg-danger tag-option">For Buy </div>
															</div>
															<div class="item-trans-rating">
																<div class="rating-stars block">
																	<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value"  value="4">
																	<div class="rating-stars-container">
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																		<div class="rating-star sm">
																			<i class="fa fa-star"></i>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="card-body">
															<div class="item-card9">
																<a href="col-left.html" class="text-muted mr-4"><i class="fa fa-tag mr-1"></i> Garden House</a>
																<a href="#" class=""><span class="text-muted"><i class="fa fa-user text-muted mr-1"></i> Owner</span></a>
																<a href="col-left.html" class="text-dark mt-2"><h4 class="font-weight-bold mt-1">Graden House</h4></a>
																<ul class="item-card2-list">
																	<li><a href="#" class="icons text-muted"><i class="fa fa-arrows-alt text-muted mr-1"></i> 489 Sqft</a></li>
																	<li><a href="#" class="icons text-muted"><i class="fa fa-bed text-muted mr-1"></i> 5 Beds</a></li>
																	<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a></li>
																	<li class="mb-3"><a href="#" class="icons text-muted"><i class="fa fa-car text-muted mr-1"></i> 1 Car</a></li>
																</ul>
																<p class="mb-0">Ut enim ad minima veniamq nostrum exerci</p>
															</div>
														</div>
														<div class="card-footer">
															<div class="item-card9-footer d-flex">
																<div class="item-card9-cost">
																	<a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a>
																</div>
																<div class="ml-auto">
																	<a href="#" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 15 mins ago</span></a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="center-block text-center">
									<ul class="pagination mb-5 mb-lg-0">
										<li class="page-item page-prev disabled">
											<a class="page-link" href="#" tabindex="-1">Prev</a>
										</li>
										<li class="page-item active"><a class="page-link" href="#">1</a></li>
										<li class="page-item"><a class="page-link" href="#">2</a></li>
										<li class="page-item"><a class="page-link" href="#">3</a></li>
										<li class="page-item page-next">
											<a class="page-link" href="#">Next</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						{{-- /Add lists --}}
					</div>

					{{-- Right Side Content --}}
					<div class="col-xl-3 col-lg-4 col-md-12">
						<div class="card">
							<div class="card-body">
								<div class="input-group">
									<input type="text" class="form-control br-tl-3  br-bl-3" placeholder="Search">
									<div class="input-group-append ">
										<button type="button" class="btn btn-primary br-tr-3  br-br-3">
											Search
										</button>
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Categories</h3>
							</div>
							<div class="card-body">
								<div class="" id="container">
									<div class="filter-product-checkboxs">
										<label class="custom-control custom-checkbox mb-3">
											<input type="checkbox" class="custom-control-input" name="checkbox1" value="option1">
											<span class="custom-control-label">
												<span class="text-dark"> Homes<span class="label label-secondary float-right">14</span></span>
											</span>
										</label>
										<label class="custom-control custom-checkbox mb-3">
											<input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
											<span class="custom-control-label">
												<span class="text-dark">Apartmenrs<span class="label label-secondary float-right">22</span></span>
											</span>
										</label>
										<label class="custom-control custom-checkbox mb-3">
											<input type="checkbox" class="custom-control-input" name="checkbox3" value="option3">
											<span class="custom-control-label">
												<span class="text-dark">Flats<span class="label label-secondary float-right">78</span></span>
											</span>
										</label>
										<label class="custom-control custom-checkbox mb-3">
											<input type="checkbox" class="custom-control-input" name="checkbox4" value="option3">
											<span class="custom-control-label">
												<span class="text-dark">Offices<span class="label label-secondary float-right">35</span></span>
											</span>
										</label>
										<label class="custom-control custom-checkbox mb-3">
											<input type="checkbox" class="custom-control-input" name="checkbox5" value="option3">
											<span class="custom-control-label">
												<span class="text-dark">2BHK Flats<span class="label label-secondary float-right">23</span></span>
											</span>
										</label>
										<label class="custom-control custom-checkbox mb-3">
											<input type="checkbox" class="custom-control-input" name="checkbox6" value="option3">
											<span class="custom-control-label">
												<span class="text-dark">Home & Garden<span class="label label-secondary float-right">14</span></span>
											</span>
										</label>
										<label class="custom-control custom-checkbox mb-3">
											<input type="checkbox" class="custom-control-input" name="checkbox7" value="option3">
											<span class="custom-control-label">
												<span class="text-dark">Luxury Homes<span class="label label-secondary float-right">45</span></span>
											</span>
										</label>
										<label class="custom-control custom-checkbox mb-3">
											<input type="checkbox" class="custom-control-input" name="checkbox7" value="option3">
											<span class="custom-control-label">
												<span class="text-dark">Apartments<span class="label label-secondary float-right">34</span></span>
											</span>
										</label>
										<label class="custom-control custom-checkbox mb-3">
											<input type="checkbox" class="custom-control-input" name="checkbox7" value="option3">
											<span class="custom-control-label">
												<span class="text-dark">Luxury Rooms<span class="label label-secondary float-right">12</span></span>
											</span>
										</label>
										<label class="custom-control custom-checkbox mb-3">
											<input type="checkbox" class="custom-control-input" name="checkbox7" value="option3">
											<span class="custom-control-label">
												<span class="text-dark">Deluxe Apartments<span class="label label-secondary float-right">18</span></span>
											</span>
										</label>
										<label class="custom-control custom-checkbox mb-3">
											<input type="checkbox" class="custom-control-input" name="checkbox7" value="option3">
											<span class="custom-control-label">
												<span class="text-dark">3BHK Homes<span class="label label-secondary float-right">02</span></span>
											</span>
										</label>
									</div>
								</div>
							</div>
							<div class="card-header border-top">
								<h3 class="card-title">Price Range</h3>
							</div>
							<div class="card-body">
								<h6>
								   <label for="price">Price Range:</label>
								   <input type="text" id="price">
								</h6>
								<div id="mySlider"></div>
							</div>
							<div class="card-header border-top">
								<h3 class="card-title">Condition</h3>
							</div>
							<div class="card-body">
								<div class="filter-product-checkboxs">
									<label class="custom-control custom-checkbox mb-2">
										<input type="checkbox" class="custom-control-input" name="checkbox1" value="option1">
										<span class="custom-control-label">
											For Rent
										</span>
									</label>
									<label class="custom-control custom-checkbox mb-2">
										<input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
										<span class="custom-control-label">
											For Sale
										</span>
									</label>
									<label class="custom-control custom-checkbox mb-0">
										<input type="checkbox" class="custom-control-input" name="checkbox3" value="option3">
										<span class="custom-control-label">
											For Buy
										</span>
									</label>
								</div>
							</div>
							<div class="card-header border-top">
								<h3 class="card-title">Posted By</h3>
							</div>
							<div class="card-body">
								<div class="filter-product-checkboxs">
									<label class="custom-control custom-checkbox mb-2">
										<input type="checkbox" class="custom-control-input" name="checkbox1" value="option1">
										<span class="custom-control-label">
											Owner
										</span>
									</label>
									<label class="custom-control custom-checkbox mb-2">
										<input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
										<span class="custom-control-label">
											Agent
										</span>
									</label>
									<label class="custom-control custom-checkbox mb-0">
										<input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
										<span class="custom-control-label">
											Builder
										</span>
									</label>
								</div>
							</div>
							<div class="card-footer">
								<a href="#" class="btn btn-primary btn-block">Apply Filter</a>
							</div>
						</div>
						<div class="card mb-0">
							<div class="card-header">
								<h3 class="card-title">Shares</h3>
							</div>
							<div class="card-body product-filter-desc">
								<div class="product-filter-icons text-center">
									<a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a>
									<a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a>
									<a href="#" class="google-bg"><i class="fa fa-google"></i></a>
									<a href="#" class="dribbble-bg"><i class="fa fa-dribbble"></i></a>
									<a href="#" class="pinterest-bg"><i class="fa fa-pinterest"></i></a>
								</div>
							</div>
						</div>
					</div>
					{{-- /Right Side Content --}}
				</div>
			</div>
		</section>
		{{-- / Add Listings --}}

		{{-- Newsletter --}}
		<section class="sptb bg-white border-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-7 col-xl-6 col-md-12">
						<div class="sub-newsletter">
							<h3 class="mb-2"><i class="fa fa-paper-plane-o mr-2"></i> Subscribe To Our Newsletter</h3>
							<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
						</div>
					</div>
					<div class="col-lg-5 col-xl-6 col-md-12">
						<div class="input-group sub-input mt-1">
							<input type="text" class="form-control input-lg " placeholder="Enter your Email">
							<div class="input-group-append ">
								<button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3">
									Subscribe
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		{{-- /Newsletter --}}
